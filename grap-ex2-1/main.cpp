/* 
 * File:   main.cpp
 * Author: Vincent Wochnik <v.wochnik@gmail.com>
 *
 * Created on 11. September 2014, 14:04
 */
#include <stdlib.h>
#include <math.h>
#include <GLUT/glut.h>

void mkVertices(GLfloat *vertices, size_t n, float r);

double offset = 0.0;

GLfloat colors[] = {
    1.0, 0.0, 0.0,
    0.5, 0.5, 0.0,
    0.0, 0.1, 0.0,
    0.0, 0.5, 0.5,
    0.0, 0.0, 1.0,
    1.0, 1.0, 1.0
};

void init(void) {
    glClearColor(0.0, 0.0, 0.0, 0.0);
}

void animate(int value) {
    glutTimerFunc(40, animate, 0);
    
    offset += M_PI / 25.0;
    if (offset >= 2.0*M_PI)
        offset -= 2.0*M_PI;
    
    glutPostRedisplay();
}

void display(void) {
    GLfloat vertices[18];

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);

    glPushMatrix();

    glTranslatef(0,0,-2);
    
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);

    mkVertices(vertices, 6, sin(offset));
    glColorPointer(3, GL_FLOAT, 0, colors);
    glVertexPointer(3, GL_FLOAT, 0, vertices);
    glDrawArrays(GL_POLYGON, 0, 6);

    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);
    glPopMatrix();

    glFlush();
}

void reshape(int w, int h) {
    glViewport(0, 0, (GLsizei) w, (GLsizei) h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, (GLfloat) w / (GLfloat) h, 1.0, 20.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void mouse(int button, int state, int x, int y) {
/*    switch (button) {
        case GLUT_LEFT_BUTTON:
            if (state == GLUT_DOWN)
            break;
        case GLUT_RIGHT_BUTTON:
            if (state == GLUT_DOWN)
            break;
        default:
            break;
    }*/
}

void keyboard(unsigned char key, int x, int y) {
    switch (key) {
        case 27:
            exit(0);
            break;
    }
}

int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(500, 500);
    glutInitWindowPosition(100, 100);
    glutCreateWindow(argv[0]);
    init();
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutMouseFunc(mouse);
    glutKeyboardFunc(keyboard);
    glutTimerFunc(40, animate, 0);
    glutMainLoop();
    return 0;
}

void mkVertices(GLfloat *vertices, size_t n, float r)
{
    double offset, increment;
    float x, y;
    size_t i, a;
    
    increment = (2.0*M_PI)/(double)n;
    
    offset = 0;
    for (i = 0; i < n; i++) {
        float x = sin(offset);
        x = sin(offset)*r;
        y = -cos(offset)*r;
        a = i*3;
        vertices[a++] = x;
        vertices[a++] = y;
        vertices[a++] = 0.0f;
        offset += increment;
    }
}
