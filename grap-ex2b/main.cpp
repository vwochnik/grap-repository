/* 
 * File:   main.cpp
 * Author: Vincent Wochnik <v.wochnik@gmail.com>
 *
 * Created on 11. September 2014, 14:04
 */
#include <stdlib.h>
#include <math.h>
#include <GLUT/glut.h>

typedef struct {
    GLfloat ambient[4];
    GLfloat diffuse[4];
    GLfloat specular[4];
    GLfloat shininess;
} material;

material silver = {{0.19225, 0.19225, 0.19225, 1.0}, {0.50754, 0.50754, 0.50754, 1.0}, {0.508273, 0.508273, 0.508273, 1.0}, 0.4};
material cyanPlastic = {{0.0, 0.1, 0.06, 1.0}, {0.0, 0.50980392, 0.50980392, 1.0}, {0.50196078, 0.50196078, 0.50196078, 1.0}, .25};
material redPlastic = {{0.0, 0.0, 0.0, 1.0}, {0.5, 0.0, 0.0, 1.0}, {0.7, 0.6, 0.6, 1.0}, .25};

void setMaterial(material*);
void drawRedCone();
void drawCyanCone();

GLfloat light0pos[] = {1.8, -0.6, 3.0, 0.0};
GLfloat light0ambient[] = {1.0, 1.0, 1.0, 1.0};

GLfloat rotation = 0.0;
GLfloat viewAngle = 50.0;

GLUquadricObj *quadObj;

void init(void) {
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE);
    glEnable(GL_MULTISAMPLE_ARB);
// Enable depth test
glEnable(GL_DEPTH_TEST);
// Accept fragment if it closer to the camera than the former one
glDepthFunc(GL_LESS);

    glLightfv(GL_LIGHT0, GL_AMBIENT, light0ambient);

    quadObj = gluNewQuadric();
    gluQuadricNormals(quadObj, GLU_SMOOTH);
    gluQuadricTexture(quadObj, GL_TRUE);
}

void cleanup()
{
    gluDeleteQuadric(quadObj);
}

void animate(int value) {
    glutTimerFunc(20, animate, 0);
    
    rotation += 5.0;
    if (rotation >= 180.0)
        rotation -= 360.0;

    glutPostRedisplay();
}

void display(void) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glPushMatrix();
    glTranslatef(0,0,-3);
    
    glLightfv(GL_LIGHT0, GL_POSITION, light0pos);

    glPushMatrix();
    glRotatef(-viewAngle, 0.0, 0.0, 0.0);

    //glRotatef(5.0, 1.0, 0.0, 0.0);
    glBegin(GL_POLYGON);
    setMaterial(&silver);
    gluCylinder(quadObj, 1.0, 1.0, 0.4, 5000, 500);
    glEnd();
    glTranslatef(0.0, 0.0, 0.4);
    
    glRotatef(-rotation, 0.0, 0.0, 1.0);

    if (rotation > -45.0) {
        drawRedCone();
        drawCyanCone();
    } else {
        drawCyanCone();
        drawRedCone();
    }

    glPopMatrix();
    glPopMatrix();

    glutSwapBuffers();
}

void reshape(int w, int h) {
    glViewport(0, 0, (GLsizei) w, (GLsizei) h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, (GLfloat) w / (GLfloat) h, 0.01, 20.0);
}

void mouse(int button, int state, int x, int y) {
/*    switch (button) {
        case GLUT_LEFT_BUTTON:
            if (state == GLUT_DOWN)
            break;
        case GLUT_RIGHT_BUTTON:
            if (state == GLUT_DOWN)
            break;
        default:
            break;
    }*/
}

void keyboard(unsigned char key, int x, int y) {
    switch (key) {
        case 27:
            exit(0);
            break;
    }
}

void specialInput(int key, int x, int y)
{
    switch (key) {
        case GLUT_KEY_UP:
            viewAngle -= 5.0;
            if (viewAngle < 0.0)
                viewAngle = 0.0;
            glutPostRedisplay();
            break;
        case GLUT_KEY_DOWN:
            viewAngle += 5.0;
            if (viewAngle >= 80.0)
                viewAngle = 80.0;
            glutPostRedisplay();
            break;
    }
}

int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH | GLUT_MULTISAMPLE);
    glutInitWindowSize(500, 500);
    glutInitWindowPosition(100, 100);
    glutCreateWindow(argv[0]);
    init();
    atexit(cleanup);
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutMouseFunc(mouse);
    glutKeyboardFunc(keyboard);
    glutSpecialFunc(specialInput);
    glutTimerFunc(20, animate, 0);
    glutMainLoop();
    return 0;
}

void setMaterial(material *mat)
{
  glMaterialfv(GL_FRONT, GL_AMBIENT, mat->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE, mat->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR, mat->specular);
  glMaterialf(GL_FRONT, GL_SHININESS, mat->shininess * 128.0);
}

void drawRedCone()
{
    glPushMatrix();
    glTranslatef(-0.4, 0.0, 0.0);
    glRotatef(rotation+180.0, 0.0, 0.0, 1.0);
    glBegin(GL_POLYGON);
    setMaterial(&redPlastic);
    gluCylinder(quadObj, 0.2, 0.0, 0.8, 30, 500);
    glEnd();
    glPopMatrix();
}

void drawCyanCone()
{
    glPushMatrix();
    glTranslatef(0.4, 0.0, 0.0);
    glRotatef(rotation+180, 0.0, 0.0, 1.0);
    glBegin(GL_POLYGON);
    setMaterial(&cyanPlastic);
    gluCylinder(quadObj, 0.2, 0.0, 0.8, 30, 500);
    glEnd();
    glPopMatrix();
}
