(function($, wnd) {
  'use strict';

  var $container, width, height,
      scene, renderer, camera, cube, time;

  function init() {
    scene = new THREE.Scene();

    renderer = new THREE.WebGLRenderer();
    $container.append(renderer.domElement);

    camera = new THREE.PerspectiveCamera(75, 1.0, 0.1, 1000);
    camera.position.setZ(2);

    time = new Date();
  }

  function makeCube() {
    var geometry = new THREE.BoxGeometry( 1, 1, 1 );
    var material = new THREE.MeshNormalMaterial();
    cube = new THREE.Mesh( geometry, material );
    scene.add(cube);
  }

  function resize() {
    width = $(wnd).innerWidth();
    height = $(wnd).innerHeight();
    camera.aspect = width / height;
    camera.updateProjectionMatrix();
    renderer.setSize(width, height);
  }

  function animate() {
    var delta = ((new Date()) - time) / 1000.0;
    var scale = 1 + Math.sin(delta) / 2.0;

    cube.rotation.x = delta * Math.PI;
    cube.rotation.y = delta * Math.PI;
    cube.scale.x = scale;
    cube.scale.y = scale;
    cube.scale.z = scale;
  }

  function render() {
    renderer.render(scene, camera);
  }

  $(function() {
    $container = $('#container');
    $(window).resize(resize);
    init();
    makeCube();
    resize();

    (function animationFrame() {
      animate();
      render();
      requestAnimationFrame(animationFrame);
    })();
  });
})(jQuery, window);
