/* 
 * File:   main.cpp
 * Author: Vincent Wochnik <v.wochnik@gmail.com>
 *
 * Created on 11. September 2014, 14:04
 */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <GLUT/glut.h>

#include "cpts.h"

void drawCircularArc(GLfloat radius, GLfloat startAngle, GLfloat endAngle);
void circularArcCpts(GLfloat *vertices, GLfloat radius, GLfloat startAngle, GLfloat endAngle);
void setCoords3f(GLfloat* vertex, GLfloat radius, GLfloat angle);

void init(void) {
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glEnable(GL_MAP1_VERTEX_3);
}

void animate(int value) {
    glutTimerFunc(20, animate, 0);

    glutPostRedisplay();
}

void display(void) {
    GLfloat d;
    
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glTranslatef(0.0, 0.0, -3.0);

    glPointSize(5.0);
    
    // draw light red
    glColor3f(1.0, 0.6, 0.6);
    glBegin(GL_LINE_LOOP);
    for (GLfloat i = 0; i < 2 * M_PI; i += M_PI / 360.0f)
        glVertex3f(cos(i), sin(i), .0f);
    glEnd();
    
    // draw light green
    glColor3f(0.6, 1.0, 0.6);
    for (d = 0; d < 360.0f; d += 90.0f)
        drawCircularArc(1, d, d+90.0f);

    glutSwapBuffers();
}

void reshape(int w, int h) {
    glViewport(0, 0, (GLsizei) w, (GLsizei) h);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    gluPerspective(60.0, (GLfloat)w / (GLfloat)h, 1.0, 20.0);
}

void mouse(int button, int state, int x, int y) {
}

void keyboard(unsigned char key, int x, int y) {
    switch (key) {
        case 27:
            exit(0);
            break;
    }
}

int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(500, 500);
    glutInitWindowPosition(100, 100);
    glutCreateWindow(argv[0]);
    init();
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutMouseFunc(mouse);
    glutKeyboardFunc(keyboard);
    //glutTimerFunc(20, animate, 0);
    glutMainLoop();
    return 0;
}

void drawCircularArc(GLfloat radius, GLfloat startAngle, GLfloat endAngle)
{
    GLfloat vertices[9], *vertex; GLsizei i;
    
    circularArcCpts(vertices, radius, startAngle, endAngle);

    // draw line
    glBegin(GL_LINE_STRIP);
    for (i = 0; i < 3; i++)
        glVertex3fv(vertices + i*3);
    glEnd();

    // draw points
    glBegin(GL_POINTS);
    for (i = 0; i < 3; i++)
        glVertex3fv(vertices + i*3);
    glEnd();

    // draw bezier curve
    glMap1f(GL_MAP1_VERTEX_3, 0.0, 1.0, 3, 3, vertices);
    glMapGrid1f(30, 0.0, 1.0);
    glEvalMesh1(GL_LINE, 0, 30);
}

void circularArcCpts(GLfloat *vertices, GLfloat radius, GLfloat startAngle, GLfloat endAngle)
{
    GLfloat midAngle, angle;

    angle = abs(endAngle - startAngle);
    midAngle = startAngle + angle / 2.0;

    // set control points
    setCoords3f(vertices, radius, startAngle);
    setCoords3f(vertices+3, radius*1.3f, midAngle);
    setCoords3f(vertices+6, radius, endAngle);
}

void setCoords3f(GLfloat* vertex, GLfloat radius, GLfloat angle)
{
    vertex[0] = radius * sin(angle * M_PI / 180.0f);
    vertex[1] = -radius * cos(angle * M_PI / 180.0f);
    vertex[2] = .0f;
}
