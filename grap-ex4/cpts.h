/* 
 * File:   cpts.h
 * Author: Vincent Wochnik <v.wochnik@gmail.com>
 *
 * Created on 1. Oktober 2014, 14:54
 */

#ifndef CPTS_H
#define	CPTS_H

#define OUTER_CPTS  31           /* Fixed maximum number of control points for outer contour */
#define INNER_CPTS  16           /* Fixed maximum number of control points for inner contour */

// letter b
// author J. Jacobs
GLfloat cpts[OUTER_CPTS+INNER_CPTS][3]={
//outer control points
                                        {1.0,27.3,0.0},{0.5,28.0,0.0},{1.0,27.3,0.0},		//left top serif
                                        {0.5,28.0,0.0},{6.2,30.5,0.0},{0.5,28.0,0.0},		//upper stroke left
                                        {6.2,30.5,0.0},{7.1,30.5,0.0},{6.2,30.5,0.0},		//upper stroke horizontal
                                        {7.1,30.5,0.0},{7.1,17.1,0.0},{7.1,30.5,0.0},		//upper right stroke down
                                        {7.1,17.1,0.0},{9.7,20.7,0.0},{12.5,20.7,0.0},		//upper left belly outside
                                        {13.2,20.7,0.0},{16.0,20.7,0.0},{20.2,20.7,0.0},	//upper right belly outside
                                        {20.2,11.5,0.0},{20.2,0.6,0.0},{13.0,0.6,0.0},		//lower right belly outside
                                        {10.3,0.6,0.0},{8.0,0.6,0.0},{6.2,0.6,0.0},			//lower left belly outside
                                        {3.7,3.2,0.0},{3.7,24.0,0.0},{3.7,3.2,0.0},			//left stroke up
                                        {3.7,24.0,0.0},{3.7,28.8,0.0},{3.7,28.8,0.0},		//closing left top serif
                                        {1.0,27.3,0.0},
//inner control points
                                        {7.1,15.4,0.0},{9.4,17.8,0.0},{11.1,17.8,0.0},		//upper left belly inside
                                        {11.8,17.8,0.0},{13.7,17.8,0.0},{16.7,17.8,0.0},	//upper right belly inside
                                        {16.7,10.5,0.0},{16.7,2.2,0.0},{13.7,2.2,0.0},		//lower right belly inside
                                        {11.0,2.2,0.0},{10.0,2.2,0.0},{9.0,2.2,0.0},			//lower left belly inside
                                        {7.1,4.2,0.0},{7.1,15.4,0.0},{7.1,4.2,0.0},			//closing left stem inside
                                        {7.1,15.4,0.0}
};


#endif	/* CPTS_H */

