/* 
 * File:   main.cpp
 * Author: Vincent Wochnik <v.wochnik@gmail.com>
 *
 * Created on 11. September 2014, 14:04
 */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <GLUT/glut.h>

GLuint loadRawTexture(const char *filename, GLsizei width, GLsizei height, bool wrap);
void drawPolygon(int a, int b, int c, int d);

GLfloat vertices[][3] = {{-1.0,-1.0,-1.0},{1.0,-1.0,-1.0},
                         {1.0,1.0,-1.0}, {-1.0,1.0,-1.0}, {-1.0,-1.0,1.0},
                         {1.0,-1.0,1.0}, {1.0,1.0,1.0}, {-1.0,1.0,1.0}};

GLfloat light0pos[] = {1.8, -0.6, 3.0, 0.0};
GLfloat light0ambient[] = {1.0, 1.0, 1.0, 1.0};
GLuint texture1, texture2;

GLfloat rotation = 0.0;
GLfloat viewAngle = 50.0;

void init(void) {
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
// Enable depth test
glEnable(GL_DEPTH_TEST);
// Accept fragment if it closer to the camera than the former one
glDepthFunc(GL_LESS);

// when texture area is small, bilinear filter the closest mipmap
glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                 GL_LINEAR_MIPMAP_NEAREST );
// when texture area is large, bilinear filter the original
glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

// the texture wraps over at the edges (repeat)
glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );

    glLightfv(GL_LIGHT0, GL_AMBIENT, light0ambient);
    glGenTextures(1, &texture1);
    glGenTextures(1, &texture2);
    texture1 = loadRawTexture("texture1.rgb", 128, 128, true);
    texture2 = loadRawTexture("texture2.rgb", 128, 128, true);
}

void cleanup()
{
    if (texture1 != 0)
        glDeleteTextures(1, &texture1);
    if (texture2 != 0)
        glDeleteTextures(1, &texture2);
}

void animate(int value) {
    glutTimerFunc(20, animate, 0);
    
    rotation += 2.5;
    if (rotation >= 180.0)
        rotation -= 360.0;

    glutPostRedisplay();
}

void display(void) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glPushMatrix();
    glTranslatef(0,0,-4);

    glPushMatrix();
    glRotatef(-viewAngle, 0.0, 0.0, 0.0);

    glRotatef(-rotation, 0.0, 0.0, 1.0);

    glBindTexture(GL_TEXTURE_2D, texture1);
    drawPolygon(0, 3, 2, 1);
    glBindTexture(GL_TEXTURE_2D, texture2);
    drawPolygon(2, 3, 7, 6);
    glBindTexture(GL_TEXTURE_2D, texture1);
    drawPolygon(3, 0, 4, 7);
    glBindTexture(GL_TEXTURE_2D, texture2);
    drawPolygon(1, 2, 6, 5);
    glBindTexture(GL_TEXTURE_2D, texture1);
    drawPolygon(4, 5, 6, 7);
    glBindTexture(GL_TEXTURE_2D, texture2);
    drawPolygon(5, 4, 0, 1);
    
    glPopMatrix();
    glLightfv(GL_LIGHT0, GL_POSITION, light0pos);
    glPopMatrix();
    
    glutSwapBuffers();
}

void reshape(int w, int h) {
    glViewport(0, 0, (GLsizei) w, (GLsizei) h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, (GLfloat) w / (GLfloat) h, 1.0, 20.0);
}

void mouse(int button, int state, int x, int y) {
/*    switch (button) {
        case GLUT_LEFT_BUTTON:
            if (state == GLUT_DOWN)
            break;
        case GLUT_RIGHT_BUTTON:
            if (state == GLUT_DOWN)
            break;
        default:
            break;
    }*/
}

void keyboard(unsigned char key, int x, int y) {
    switch (key) {
        case 27:
            exit(0);
            break;
    }
}

void specialInput(int key, int x, int y)
{
    switch (key) {
        case GLUT_KEY_UP:
            viewAngle -= 5.0;
            if (viewAngle < 0.0)
                viewAngle = 0.0;
            glutPostRedisplay();
            break;
        case GLUT_KEY_DOWN:
            viewAngle += 5.0;
            if (viewAngle >= 80.0)
                viewAngle = 80.0;
            glutPostRedisplay();
            break;
    }
}

int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(500, 500);
    glutInitWindowPosition(100, 100);
    glutCreateWindow(argv[0]);
    init();
    atexit(cleanup);
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutMouseFunc(mouse);
    glutKeyboardFunc(keyboard);
    glutSpecialFunc(specialInput);
    glutTimerFunc(20, animate, 0);
    glutMainLoop();
    return 0;
}

GLuint loadRawTexture(const char *filename, GLsizei width, GLsizei height, bool wrap)
{
    GLuint texture;
    FILE *fp;
    char *data;

    // open texture data
    fp = fopen(filename, "rb");
    if (fp == NULL)
        return 0;

    data = (char*)malloc(width*height*3*sizeof(char));

    // read texture data
    if (fread(data, width*height*3, 1, fp) != 1) {
        fclose(fp);
        return 0;
    }
    fclose(fp);

    // allocate a texture name
    glGenTextures(1, &texture);
    
    // select our current texture
    glBindTexture( GL_TEXTURE_2D, texture );

    // select modulate to mix texture with color for shading
    glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );

    // when texture area is small, bilinear filter the closest mipmap
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                     GL_LINEAR_MIPMAP_NEAREST );
    // when texture area is large, bilinear filter the first mipmap
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

    // if wrap is true, the texture wraps over at the edges (repeat)
    //       ... false, the texture ends at the edges (clamp)
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,
                     wrap ? GL_REPEAT : GL_CLAMP );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,
                     wrap ? GL_REPEAT : GL_CLAMP );

    // build our texture mipmaps
    gluBuild2DMipmaps( GL_TEXTURE_2D, 3, width, height,
                       GL_RGB, GL_UNSIGNED_BYTE, data );

    // free buffer
    free( data );

    return texture;
}

void drawPolygon(int a, int b, int c, int d)
{
	/* draw a polygon via list of vertices */
	glEnable(GL_TEXTURE_2D);
	glBegin(GL_POLYGON);
	glVertex3fv(vertices[a]);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3fv(vertices[b]);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3fv(vertices[c]);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3fv(vertices[d]);
	glTexCoord2f(0.0f, 1.0f);
	glEnd();
	glDisable(GL_TEXTURE_2D);
}
