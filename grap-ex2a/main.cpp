/* 
 * File:   main.cpp
 * Author: Vincent Wochnik <v.wochnik@gmail.com>
 *
 * Created on 11. September 2014, 14:04
 */
#include <stdlib.h>
#include <math.h>
#include <GLUT/glut.h>

void init(void) {
    glClearColor(0.0, 0.0, 0.0, 0.0);
}

void animate(int value) {
    glutTimerFunc(20, animate, 0);

    glutPostRedisplay();
}

void display(void) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glPushMatrix();
    glTranslatef(0,0,-3);
    glRotatef(-70.0, 0, 0, 0.0);

    glRotatef(-12.5, 0.0, 1.0, 0.0);
    glColor3f(0.0, 1.0, 0.0);

    glPushMatrix();
    glTranslatef(-1.0, 0.0, 0.0);
    glScalef(0.6, 0.6, 0.6);
    glutWireCube(sqrt(3)/2.0);
    glPopMatrix();

    glPushMatrix();
    glScalef(0.6, 0.6, 0.6);
    glRotatef(-45.0, 0.0, 1.0, 0.0);
    glutWireTetrahedron();
    glPopMatrix();

    glPushMatrix();
    glTranslatef(1.0, 0.0, 0.0);
    glScalef(0.8, 0.6, 0.6);
    glutWireSphere(sqrt(3)/3.0, 8, 8);
    glPopMatrix();

    glPopMatrix();
    glutSwapBuffers();
}

void reshape(int w, int h) {
    glViewport(0, 0, (GLsizei) w, (GLsizei) h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, (GLfloat) w / (GLfloat) h, 1.0, 20.0);
}

void mouse(int button, int state, int x, int y) {
/*    switch (button) {
        case GLUT_LEFT_BUTTON:
            if (state == GLUT_DOWN)
            break;
        case GLUT_RIGHT_BUTTON:
            if (state == GLUT_DOWN)
            break;
        default:
            break;
    }*/
}

void keyboard(unsigned char key, int x, int y) {
    switch (key) {
        case 27:
            exit(0);
            break;
    }
}

int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(500, 500);
    glutInitWindowPosition(100, 100);
    glutCreateWindow(argv[0]);
    init();
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutMouseFunc(mouse);
    glutKeyboardFunc(keyboard);
    glutTimerFunc(20, animate, 0);
    glutMainLoop();
    return 0;
}
