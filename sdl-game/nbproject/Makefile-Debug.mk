#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-MacOSX
CND_DLIB_EXT=dylib
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/src/app/Application.o \
	${OBJECTDIR}/src/fp/Background.o \
	${OBJECTDIR}/src/fp/Camera.o \
	${OBJECTDIR}/src/fp/Character.o \
	${OBJECTDIR}/src/fp/FirstPersonGameState.o \
	${OBJECTDIR}/src/fp/FirstPersonPerspective.o \
	${OBJECTDIR}/src/fp/Floor.o \
	${OBJECTDIR}/src/fp/GameTimer.o \
	${OBJECTDIR}/src/fp/Gravity.o \
	${OBJECTDIR}/src/fp/HeadUpDisplay.o \
	${OBJECTDIR}/src/fp/HelicopterPerspective.o \
	${OBJECTDIR}/src/fp/Player.o \
	${OBJECTDIR}/src/fp/PlayerControl.o \
	${OBJECTDIR}/src/fp/PlayerMovement.o \
	${OBJECTDIR}/src/fp/Sphere.o \
	${OBJECTDIR}/src/fp/Terrain.o \
	${OBJECTDIR}/src/fp/Tree.o \
	${OBJECTDIR}/src/fp/World.o \
	${OBJECTDIR}/src/fp/WorldLight.o \
	${OBJECTDIR}/src/game/GameContext.o \
	${OBJECTDIR}/src/game/GameState.o \
	${OBJECTDIR}/src/main.o \
	${OBJECTDIR}/src/math/FractalTreeGenerator.o \
	${OBJECTDIR}/src/math/VectorTransformation.o \
	${OBJECTDIR}/src/math/Velocity.o \
	${OBJECTDIR}/src/obj/AbstractSphere.o \
	${OBJECTDIR}/src/res/ImageTexture.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=`/usr/local/bin/sdl-config --cflags` 
CXXFLAGS=`/usr/local/bin/sdl-config --cflags` 

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/sdl-game

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/sdl-game: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/sdl-game ${OBJECTFILES} ${LDLIBSOPTIONS} `/usr/local/bin/sdl-config --libs` -Dmain=SDL_main -framework OpenGL -lSOILdas

${OBJECTDIR}/src/app/Application.o: src/app/Application.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/app
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/app/Application.o src/app/Application.cpp

${OBJECTDIR}/src/fp/Background.o: src/fp/Background.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/fp
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/fp/Background.o src/fp/Background.cpp

${OBJECTDIR}/src/fp/Camera.o: src/fp/Camera.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/fp
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/fp/Camera.o src/fp/Camera.cpp

${OBJECTDIR}/src/fp/Character.o: src/fp/Character.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/fp
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/fp/Character.o src/fp/Character.cpp

${OBJECTDIR}/src/fp/FirstPersonGameState.o: src/fp/FirstPersonGameState.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/fp
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/fp/FirstPersonGameState.o src/fp/FirstPersonGameState.cpp

${OBJECTDIR}/src/fp/FirstPersonPerspective.o: src/fp/FirstPersonPerspective.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/fp
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/fp/FirstPersonPerspective.o src/fp/FirstPersonPerspective.cpp

${OBJECTDIR}/src/fp/Floor.o: src/fp/Floor.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/fp
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/fp/Floor.o src/fp/Floor.cpp

${OBJECTDIR}/src/fp/GameTimer.o: src/fp/GameTimer.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/fp
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/fp/GameTimer.o src/fp/GameTimer.cpp

${OBJECTDIR}/src/fp/Gravity.o: src/fp/Gravity.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/fp
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/fp/Gravity.o src/fp/Gravity.cpp

${OBJECTDIR}/src/fp/HeadUpDisplay.o: src/fp/HeadUpDisplay.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/fp
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/fp/HeadUpDisplay.o src/fp/HeadUpDisplay.cpp

${OBJECTDIR}/src/fp/HelicopterPerspective.o: src/fp/HelicopterPerspective.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/fp
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/fp/HelicopterPerspective.o src/fp/HelicopterPerspective.cpp

${OBJECTDIR}/src/fp/Player.o: src/fp/Player.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/fp
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/fp/Player.o src/fp/Player.cpp

${OBJECTDIR}/src/fp/PlayerControl.o: src/fp/PlayerControl.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/fp
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/fp/PlayerControl.o src/fp/PlayerControl.cpp

${OBJECTDIR}/src/fp/PlayerMovement.o: src/fp/PlayerMovement.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/fp
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/fp/PlayerMovement.o src/fp/PlayerMovement.cpp

${OBJECTDIR}/src/fp/Sphere.o: src/fp/Sphere.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/fp
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/fp/Sphere.o src/fp/Sphere.cpp

${OBJECTDIR}/src/fp/Terrain.o: src/fp/Terrain.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/fp
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/fp/Terrain.o src/fp/Terrain.cpp

${OBJECTDIR}/src/fp/Tree.o: src/fp/Tree.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/fp
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/fp/Tree.o src/fp/Tree.cpp

${OBJECTDIR}/src/fp/World.o: src/fp/World.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/fp
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/fp/World.o src/fp/World.cpp

${OBJECTDIR}/src/fp/WorldLight.o: src/fp/WorldLight.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/fp
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/fp/WorldLight.o src/fp/WorldLight.cpp

${OBJECTDIR}/src/game/GameContext.o: src/game/GameContext.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/game
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/game/GameContext.o src/game/GameContext.cpp

${OBJECTDIR}/src/game/GameContext.h.gch: src/game/GameContext.h 
	${MKDIR} -p ${OBJECTDIR}/src/game
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o "$@" src/game/GameContext.h

${OBJECTDIR}/src/game/GameState.o: src/game/GameState.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/game
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/game/GameState.o src/game/GameState.cpp

${OBJECTDIR}/src/main.o: src/main.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/main.o src/main.cpp

${OBJECTDIR}/src/math/FractalTreeGenerator.o: src/math/FractalTreeGenerator.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/math
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/math/FractalTreeGenerator.o src/math/FractalTreeGenerator.cpp

${OBJECTDIR}/src/math/VectorTransformation.o: src/math/VectorTransformation.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/math
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/math/VectorTransformation.o src/math/VectorTransformation.cpp

${OBJECTDIR}/src/math/Velocity.o: src/math/Velocity.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/math
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/math/Velocity.o src/math/Velocity.cpp

${OBJECTDIR}/src/obj/AbstractSphere.o: src/obj/AbstractSphere.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/obj
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/obj/AbstractSphere.o src/obj/AbstractSphere.cpp

${OBJECTDIR}/src/res/ImageTexture.o: src/res/ImageTexture.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/res
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/res/ImageTexture.o src/res/ImageTexture.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/sdl-game

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
