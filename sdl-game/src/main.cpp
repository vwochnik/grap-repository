#include <stdlib.h>
#include "app/Application.h"
#include <SDL/SDL_main.h>

Application *application = NULL;

void exit_func()
{
    if (application != NULL)
        application->destroy();
}

#ifdef __cplusplus
extern "C"
#endif
int SDL_main(int argc, char *argv[])
{
    atexit(exit_func);

    application = new Application(800, 600);
    if ((application == NULL) || (!application->init()))
        return 1;

    application->run();
    application->destroy();
    return 0;
}
