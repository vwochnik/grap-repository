#ifndef GAMESTATE_H
#define	GAMESTATE_H

#include "GameContext.h"
#include <SDL/SDL_stdinc.h>
#include <SDL/SDL_keysym.h>

class GameState {
public:
    GameState(GameContext&);
    virtual ~GameState() { };

    virtual void enter() = 0;
    virtual void leave() = 0;

    virtual void update(Uint32) = 0;
    virtual void display() = 0;
    virtual void reshape() = 0;
    virtual void mouseMove(Sint32, Sint32) = 0;
    virtual void mouseDown(bool) = 0;
    virtual void mouseUp(bool) = 0;
    virtual void keyDown(SDLKey) = 0;
    virtual void keyUp(SDLKey) = 0;

protected:
    GameContext& _context;
};

#endif	/* GAMESTATE_H */

