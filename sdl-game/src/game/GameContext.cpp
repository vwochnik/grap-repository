#include "GameContext.h"

GameContext::GameContext(int width, int height, int fps) :
    _width(width), _height(height), _fps(fps)
{
}

GameContext::~GameContext()
{
}

int GameContext::width()
{
    return _width;
}

int GameContext::height()
{
    return _height;
}

int GameContext::fps()
{
    return _fps;
}
