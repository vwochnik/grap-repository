#ifndef GAMECONTEXT_H
#define	GAMECONTEXT_H

class Application;

class GameContext
{
public:
    GameContext(int width, int height, int fps);
    ~GameContext();
    
    int width();
    int height();
    int fps();

private:
    int _width, _height, _fps;

friend class Application;
};

#endif	/* GAMECONTEXT_H */

