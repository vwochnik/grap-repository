/* 
 * File:   GameTimer.h
 * Author: Vincent
 *
 * Created on 24. Dezember 2014, 12:43
 */

#ifndef GAMETIMER_H
#define	GAMETIMER_H

#include <SDL/SDL_stdinc.h>

class GameTimer
{
public:
    GameTimer(Uint32);
    
    double expired();
    
    void update(Uint32);

private:
    Uint32 _time, _passed;
};

#endif	/* GAMETIMER_H */

