/* 
 * File:   FirstPersonGameState.h
 * Author: Vincent
 *
 * Created on 9. Dezember 2014, 22:00
 */

#ifndef FIRSTPERSONGAMESTATE_H
#define	FIRSTPERSONGAMESTATE_H

#include "../game/GameState.h"
#include "GameTimer.h"
#include "World.h"
#include "Player.h"
#include "PlayerControl.h"
#include "FirstPersonPerspective.h"
#include "HelicopterPerspective.h"
#include "HeadUpDisplay.h"

class FirstPersonGameState : public GameState
{
public:
    FirstPersonGameState(GameContext&);
    void enter();
    void leave();

    void update(Uint32);
    void display();
    void reshape();
    void mouseMove(Sint32, Sint32);
    void mouseDown(bool);
    void mouseUp(bool);
    void keyDown(SDLKey);
    void keyUp(SDLKey);

private:
    GameTimer _timer;
    World _world;
    PlayerControl _playerControl;
    FirstPersonPerspective _firstPersonPerspective;
    HelicopterPerspective _helicopterPerspective;
    HeadUpDisplay _headUpDisplay;
};

#endif	/* FIRSTPERSONGAMESTATE_H */

