/* 
 * File:   Floor.h
 * Author: Vincent
 *
 * Created on 18. Dezember 2014, 15:22
 */

#ifndef FLOOR_H
#define	FLOOR_H

#include <SDL/SDL_stdinc.h>
#include <OpenGL/gltypes.h>

class Floor
{
public:
    Floor(GLdouble, GLsizei, GLsizei);
    
    void update(Uint32);
    void display();

private:
    GLdouble _radius;
    GLsizei _slices, _stacks;
};

#endif	/* FLOOR_H */

