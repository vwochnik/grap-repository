/* 
 * File:   PlayerControl.cpp
 * Author: Vincent
 * 
 * Created on 10. Dezember 2014, 23:56
 */

#include "PlayerControl.h"

#define PLAYER_KEY_FORWARD  SDLK_w
#define PLAYER_KEY_BACKWARD SDLK_s
#define PLAYER_KEY_LEFT     SDLK_a
#define PLAYER_KEY_RIGHT    SDLK_d
#define PLAYER_KEY_JUMP     SDLK_SPACE

PlayerControl::PlayerControl(Player& player) :
    _player(player),
    _forwardKeyPressed(false),
    _backwardKeyPressed(false),
    _leftKeyPressed(false),
    _rightKeyPressed(false),
    _jumpKeyPressed(false)
{
}

void PlayerControl::update(Uint32 delta)
{
    if (_forwardKeyPressed)
        _player.movement().stepForward();
    if (_backwardKeyPressed)
        _player.movement().stepBackward();
    if (_leftKeyPressed)
        _player.movement().stepLeft();
    if (_rightKeyPressed)
        _player.movement().stepRight();
    if (_jumpKeyPressed)
        _player.movement().jump();
}

void PlayerControl::mouseMove(Sint32 xdelta, Sint32 ydelta)
{
    _player.movement().rotate(0.5*xdelta, -0.5*ydelta);
}

void PlayerControl::mouseDown(bool alternative)
{
    
}

void PlayerControl::mouseUp(bool alternative)
{
    
}

void PlayerControl::keyDown(SDLKey key)
{
    switch (key) {
        case PLAYER_KEY_FORWARD: _forwardKeyPressed = true; break;
        case PLAYER_KEY_BACKWARD: _backwardKeyPressed = true; break;
        case PLAYER_KEY_LEFT: _leftKeyPressed = true; break;
        case PLAYER_KEY_RIGHT: _rightKeyPressed = true; break;
        case PLAYER_KEY_JUMP: _jumpKeyPressed = true; break;
    }
}

void PlayerControl::keyUp(SDLKey key)
{
    switch (key) {
        case PLAYER_KEY_FORWARD: _forwardKeyPressed = false; break;
        case PLAYER_KEY_BACKWARD: _backwardKeyPressed = false; break;
        case PLAYER_KEY_LEFT: _leftKeyPressed = false; break;
        case PLAYER_KEY_RIGHT: _rightKeyPressed = false; break;
        case PLAYER_KEY_JUMP: _jumpKeyPressed = false; break;
    }
}
