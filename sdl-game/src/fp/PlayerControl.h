/* 
 * File:   PlayerControl.h
 * Author: Vincent
 *
 * Created on 10. Dezember 2014, 23:56
 */

#ifndef PLAYERCONTROL_H
#define	PLAYERCONTROL_H

#include "Player.h"
#include <SDL/SDL_stdinc.h>
#include <SDL/SDL_keysym.h>

class PlayerControl {
public:
    PlayerControl(Player&);
    
    void update(Uint32);
    void mouseMove(Sint32, Sint32);
    void mouseDown(bool);
    void mouseUp(bool);
    void keyDown(SDLKey);
    void keyUp(SDLKey);
private:
    Player& _player;
    bool _forwardKeyPressed,
         _backwardKeyPressed,
         _leftKeyPressed,
         _rightKeyPressed,
         _jumpKeyPressed;
};

#endif	/* PLAYERCONTROL_H */

