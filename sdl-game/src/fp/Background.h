/* 
 * File:   Background.h
 * Author: Vincent
 *
 * Created on 22. Dezember 2014, 10:41
 */

#ifndef BACKGROUND_H
#define	BACKGROUND_H

#include "Sphere.h"
#include "../res/ImageTexture.h"

class Background : public Sphere
{
public:
    Background();

    void display();

private:
    ImageTexture _texture;
};

#endif	/* BACKGROUND_H */

