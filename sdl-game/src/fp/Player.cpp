/* 
 * File:   Player.cpp
 * Author: Vincent
 * 
 * Created on 10. Dezember 2014, 23:58
 */

#include "Player.h"
#include <OpenGL/gl.h>
#include <math.h>
#include "../math/VectorTransformation.h"

Player::Player(Gravity& gravity) :
    _gravity(gravity),
    _character(2.4, 0.6)
{
    _up[0] = 0.0; _up[1] = 1.0; _up[2] = 0.0;
    _front[0] = 1.0; _front[1] = 0.0; _front[2] = 0.0;
    _position[0] = -5.0; _position[1] = -50.0; _position[2] = 0.0;
    _right[0] = 1.0; _right[1] = 0.0; _right[2] = 0.0;
}

void Player::update(Uint32 delta)
{
    GLdouble tmp[3];
    GLdouble move, strafe, yaw, pitch, roll, distance;
    
    _movement.update(delta);
    _movement.movement(move, strafe);
    _movement.rotation(yaw, pitch, roll);
    
    // rotate yaw
    VectorTransformation::copy(tmp, _front);
    VectorTransformation::rotateAroundAxis(_front, tmp, _up, -yaw);
    VectorTransformation::crossProduct(_right, _front, _up);
    
    // move
    _position[0] += move*_front[0] + strafe*_right[0];
    _position[1] += move*_front[1] + strafe*_right[1];
    _position[2] += move*_front[2] + strafe*_right[2];
    
    // update gravity direction (up direction, then derive others)
    _gravity.gravity(_up, distance, _position);
    VectorTransformation::crossProduct(_front, _up, _right);
    VectorTransformation::crossProduct(_right, _front, _up);
    
    // set player on the ground
    _position[0] -= _up[0] * distance;
    _position[1] -= _up[1] * distance;
    _position[2] -= _up[2] * distance;
    
    _character.update(_position, _up, _front, pitch, roll);
}

PlayerMovement& Player::movement()
{
    return _movement;
}

void Player::eye(GLdouble *position, GLdouble *direction, GLdouble *up)
{
    _character.eye(position, direction, up);
}

void Player::observer(GLdouble *position, GLdouble *direction, GLdouble *up)
{
    VectorTransformation::rotateAroundAxis(direction, _front, _right, -50.0);
    VectorTransformation::rotateAroundAxis(up, _up, _right, -50.0);
    
    position[0] = _position[0] - direction[0] * 20.0;
    position[1] = _position[1] - direction[1] * 20.0;
    position[2] = _position[2] - direction[2] * 20.0;
}

void Player::display()
{
    GLdouble position[3], direction[3], up[3];
    
    _character.eye(position, direction, up);

    GLfloat light1pos[] = {position[0], position[1], position[2], 1.0};
    GLfloat light1dir[] = {direction[0], direction[1], direction[2]};
    GLfloat light1diffuse[] = {0.7, 0.7, 1.0, 0.3};

    glEnable(GL_LIGHT1);
    glLightfv(GL_LIGHT1, GL_POSITION, light1pos);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, light1diffuse);
    glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, light1dir);
    glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, 30.0);
    glLightf(GL_LIGHT1, GL_SPOT_EXPONENT, 12.0);
    
    _character.display();
}