#include "World.h"
#include "Gravity.h"
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include "../math/VectorTransformation.h"

World::World() :
    _light(250.0, 30.0, 120000),
    _terrain(50.0, 1800),
    _gravity(_terrain),
    _player(_gravity)
{
    size_t i;
    GLdouble ground[3], up[3], front[3];
    
    // spawn trees
    for (i = 0; i < 9; i++) {
        _terrain.ground(ground, up, (GLdouble)i * 360.0 / 9.0, 0.0);
        VectorTransformation::orthogonal(front, up);
        _trees[i] = new Tree(ground, up, front, 1.0 + (GLdouble)i / 4.5, 30, 5, 5);
    }
}

World::~World()
{
    size_t i;

    for (i = 0; i < 9; i++)
        delete _trees[i];
}

Player& World::player()
{
    return _player;
}


void World::update(Uint32 delta)
{
    size_t i;

    _light.update(delta);
    _player.update(delta);

    for (i = 0; i < 9; i++)
        _trees[i]->update(delta);
}

void World::display(GLdouble *position, GLdouble *direction, GLdouble *up, GLdouble ratio)
{
    size_t i;

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glDepthFunc(GL_LESS);

    glColor4d(1.0, 1.0, 1.0, 1.0);
    glClearColor(1.0, 1.0, 1.0, 1.0);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60, ratio, 0.01, 300.0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(position[0], position[1], position[2], position[0]+direction[0], position[1]+direction[1], position[2]+direction[2], up[0], up[1], up[2]);

    _background.display();
    _light.display();
    _player.display();
    _terrain.display();
    _gravity.display();

    for (i = 0; i < 9; i++)
        _trees[i]->display();
}
