/* 
 * File:   Character.h
 * Author: Vincent
 *
 * Created on 19. Dezember 2014, 16:04
 */

#ifndef CHARACTER_H
#define	CHARACTER_H

#include <SDL/SDL_stdinc.h>
#include <OpenGL/gltypes.h>

class Character
{
public:
    Character(GLdouble, GLdouble);
    
    void update(GLdouble*, GLdouble*, GLdouble*, GLdouble, GLdouble);
    void eye(GLdouble*, GLdouble*, GLdouble*);
    void display();

private:
    GLdouble _position[3], _up[3], _front[3], _cross[3], _pitch, _roll;
    GLdouble _eyePosition[3], _eyeDirection[3], _eyeUp[3];
    GLdouble _shoulderHeight, _neckLength;
};

#endif	/* CHARACTER_H */

