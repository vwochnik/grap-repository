/* 
 * File:   GameTimer.cpp
 * Author: Vincent
 * 
 * Created on 24. Dezember 2014, 12:43
 */

#include "GameTimer.h"

GameTimer::GameTimer(Uint32 time) :
    _time(time), _passed(0)
{
}

double GameTimer::expired()
{
    return (_passed >= _time) ? 1.0 : ((double)_passed / (double)_time);
}

void GameTimer::update(Uint32 delta)
{
    _passed += delta;
}
