/* 
 * File:   Terrain.cpp
 * Author: Vincent
 * 
 * Created on 19. Dezember 2014, 21:10
 */

#include "Terrain.h"
#include <OpenGL/gl.h>
#include <math.h>
#include "../math/VectorTransformation.h"

GLfloat __terrain_ambient[4] = {0.6, 0.1, 0.1, 0.3};
GLfloat __terrain_diffuse[4] = {0.4, 0.1, 0.1, 0.3};
GLfloat __terrain_specular[4] = {0.4, 0.2, 0.2, 0.3};
GLfloat __terrain_shininess = 32.0;

Terrain::Terrain(GLdouble radius, GLsizei slices) :
    AbstractSphere(slices, false),
    _radius(radius),
    _texture("ground", true)
{
}

void Terrain::display()
{
    glMaterialfv(GL_FRONT, GL_AMBIENT, __terrain_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, __terrain_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, __terrain_specular);
    glMaterialf(GL_FRONT, GL_SHININESS, __terrain_shininess);
    
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, _texture.id());
    AbstractSphere::display();
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
}

void Terrain::ground(GLdouble *dst, GLdouble *up, GLdouble longitude, GLdouble latitude)
{
    // ground vector
    vector(dst, longitude, latitude);
    
    dst[0] += _position[0];
    dst[1] += _position[1];
    dst[2] += _position[2];
    
    // normal (a.k.a. up) vector is direction of gravity
    normal(up, longitude, latitude);
}

void Terrain::ground(GLdouble *dst, GLdouble *up, GLdouble *position)
{
    GLdouble tmp[3];
    GLdouble longitude, latitude;

    // relative position to sphere
    tmp[0] = position[0] - _position[0];
    tmp[1] = position[1] - _position[1];
    tmp[2] = position[2] - _position[2];
    
    // get degrees on terrain for relative position
    degrees(longitude, latitude, tmp);
    
    ground(dst, up, longitude, latitude);
}

void Terrain::height(GLdouble *h, GLdouble longitude, GLdouble latitude)
{
    GLdouble m;

    m = fabs(sin(longitude * 8 * M_PI/180.0)) * 0.3 + fabs(cos(latitude * 8 * M_PI/180.0)) * 0.7;

    // make sure there is no height difference at poles to avoid glitches
    m *= 1.0 - fabs(latitude / 90.0);
    
    *h = _radius + 0.2 * _radius * m;
}

void Terrain::vector(GLdouble* dst, GLdouble longitude, GLdouble latitude)
{
    GLdouble h;
    
    height(&h, longitude, latitude);

    dst[0] = h * cos(longitude*M_PI/180.0) * fabs(cos(latitude*M_PI/180.0));
    dst[1] = h * sin(longitude*M_PI/180.0) * fabs(cos(latitude*M_PI/180.0));
    dst[2] = h * sin(latitude*M_PI/180.0);
}

void Terrain::normal(GLdouble* dst, GLdouble longitude, GLdouble latitude)
{
    dst[0] = cos(longitude*M_PI/180.0) * fabs(cos(latitude*M_PI/180.0));
    dst[1] = sin(longitude*M_PI/180.0) * fabs(cos(latitude*M_PI/180.0));
    dst[2] = sin(latitude*M_PI/180.0);
}

void Terrain::texture(GLdouble* dst, GLdouble longitude, GLdouble latitude)
{
    dst[0] = fmod(longitude / 360.0 * 32.0, 1.0);
    dst[1] = fmod((90.0 + latitude) / 180.0 * 32.0, 1.0);
}

void Terrain::degrees(GLdouble& longitude, GLdouble& latitude, GLdouble *position)
{
    GLdouble tmp[3];

    VectorTransformation::normalize(tmp, position);

    latitude = 180.0/M_PI * asin(tmp[2]);
    longitude = 180.0/M_PI * atan2(tmp[1], tmp[0]);
    if (longitude < 0.0) longitude += 360.0;
}
