/* 
 * File:   World.h
 * Author: Vincent
 *
 * Created on 11. Dezember 2014, 09:53
 */

#ifndef WORLD_H
#define	WORLD_H

#include <SDL/SDL_stdinc.h>
#include "Player.h"
#include "Background.h"
#include "WorldLight.h"
#include "Floor.h"
#include "Tree.h"
#include "Terrain.h"
#include "Gravity.h"

class World {
public:
    World();
    ~World();
    
    Player& player();
    
    void update(Uint32);
    void display(GLdouble*, GLdouble*, GLdouble*, GLdouble);

private:
    Background _background;
    Terrain _terrain;
    Gravity _gravity;
    Player _player;
    WorldLight _light;
    Tree *_trees[9];
};

#endif	/* WORLD_H */

