#include "Tree.h"
#include <stdlib.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <math.h>
#include "../math/VectorTransformation.h"

GLfloat __tree_ambient[4] = {0.4, 0.24, 0.2, 0.4};
GLfloat __tree_diffuse[4] = {0.6, 0.4, 0.2, 0.4};
GLfloat __tree_specular[4] = {0.6, 0.5, 0.4, 0.4};
GLfloat __tree_shininess = 64.0;

Tree::Tree(GLdouble *position, GLdouble *up, GLdouble *front, GLdouble size, GLdouble angle, GLsizei depth, GLsizei branches) :
    _size(size), _angle(angle),
    _ftg(depth, branches),
    _texture("wood", true),
    _list(0), _compiled(false)
{
    _ftg.generate(position, up, front, _size, _angle, 0.8);
}

void Tree::update(Uint32 delta)
{
}

void Tree::display()
{
    glMaterialfv(GL_FRONT, GL_AMBIENT, __tree_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, __tree_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, __tree_specular);
    glMaterialf(GL_FRONT, GL_SHININESS, __tree_shininess);

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, _texture.id());

    if ((_compiled) && (_list != 0)) {
        glCallList(_list);
    } else {
        if (!_compiled)
            _list = glGenLists(1);
        if ((!_compiled) && (_list != 0))
            glNewList(_list, GL_COMPILE_AND_EXECUTE);
        render();
        if ((!_compiled) && (_list != 0))
            glEndList();
        _compiled = true;
    }

    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
}

void Tree::render()
{
    FractalTreeGenerator::Fractal *fractals;
    GLdouble distance;
    size_t i, count;

    fractals = _ftg.fractals();
    count = _ftg.count();
    distance = _ftg.distance();
    
    for (i = 0; i < count; i++)
        renderFragment(fractals[i].position, fractals[i].up, fractals[i].front,
                fractals[i].length,
                0.125 * _size * pow(1.0 - fractals[i].distance / distance, 1.0),
                0.125 * _size * pow(1.0 - (fractals[i].distance + fractals[i].length) / distance, 1.0));
}

void Tree::renderFragment(GLdouble *position, GLdouble *up, GLdouble *front, GLdouble length, GLdouble bottomr, GLdouble topr)
{
    GLdouble rotated[3], cross[3], normal[3];
    GLdouble degree;
    GLsizei i;

    degree = 180.0/M_PI * asin((topr - bottomr) / length);

    glBegin(GL_TRIANGLE_STRIP);
    for (i = 0; i <= 8; i++) {
        VectorTransformation::rotateAroundAxis(rotated, front, up, 360.0 - 360.0/8.0*(GLdouble)i);

        VectorTransformation::crossProduct(cross, up, rotated);
        VectorTransformation::rotateAroundAxis(normal, rotated, cross, degree);
        
        glNormal3dv(normal);
        glTexCoord2d((GLdouble)i/8.0, 0.0);
        glVertex3d(position[0]+rotated[0]*bottomr,
                   position[1]+rotated[1]*bottomr,
                   position[2]+rotated[2]*bottomr);

        glNormal3dv(normal);
        glTexCoord2d((GLdouble)i/8.0, 1.0);
        glVertex3d(position[0]+up[0]*length+rotated[0]*topr,
                   position[1]+up[1]*length+rotated[1]*topr,
                   position[2]+up[2]*length+rotated[2]*topr);
        
    }
    glEnd();
}