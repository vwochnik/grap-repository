/* 
 * File:   Player.h
 * Author: Vincent
 *
 * Created on 10. Dezember 2014, 23:58
 */

#ifndef PLAYER_H
#define	PLAYER_H

#include <SDL/SDL_stdinc.h>
#include "PlayerMovement.h"
#include "Character.h"
#include "Gravity.h"

class Player {
public:
    Player(Gravity&);
    
    void update(Uint32);
    PlayerMovement& movement();
    void eye(GLdouble*, GLdouble*, GLdouble*);
    void observer(GLdouble*, GLdouble*, GLdouble*);

    void display();
    
private:
    GLdouble _position[3], _up[3], _front[3], _right[3];
    Gravity& _gravity;
    
    Character _character;
    PlayerMovement _movement;
};

#endif	/* PLAYER_H */

