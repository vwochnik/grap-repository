/* 
 * File:   Tree.h
 * Author: Vincent
 *
 * Created on 16. Dezember 2014, 10:40
 */

#ifndef TREE_H
#define	TREE_H

#include <SDL/SDL_stdinc.h>
#include <OpenGL/gltypes.h>
#include "../math/FractalTreeGenerator.h"
#include "../res/ImageTexture.h"

class Tree
{
public:
    Tree(GLdouble*, GLdouble*, GLdouble*, GLdouble, GLdouble, GLsizei, GLsizei);
    
    void update(Uint32);
    void display();
    void render();
    void renderFragment(GLdouble*, GLdouble*, GLdouble*, GLdouble, GLdouble, GLdouble);

private:
    GLdouble _size, _angle;
    FractalTreeGenerator _ftg;
    ImageTexture _texture;

private:
    GLuint _list;
    bool _compiled;
};

#endif	/* TREE_H */

