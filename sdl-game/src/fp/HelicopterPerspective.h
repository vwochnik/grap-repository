/* 
 * File:   HelicopterPerspective.h
 * Author: Vincent
 *
 * Created on 15. Dezember 2014, 16:53
 */

#ifndef HELICOPTERPERSPECTIVE_H
#define	HELICOPTERPERSPECTIVE_H

#include "../game/GameContext.h"
#include "Camera.h"
#include "Player.h"
#include "World.h"
#include <SDL/SDL_stdinc.h>
#include <OpenGL/gltypes.h>

class HelicopterPerspective
{
public:
    HelicopterPerspective(GameContext&, World&, GLsizei);
    
    GLuint& texId();

    void init();
    void destroy();

    void update(Uint32);

    void display();

private:
    GameContext& _context;
    World& _world;
    GLsizei _size;
    
    GLuint _glFramebuffer, _glRenderbuffer, _glTexture;
};

#endif	/* HELICOPTERPERSPECTIVE_H */

