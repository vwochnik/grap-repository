/* 
 * File:   PlayerMovement.cpp
 * Author: Vincent
 * 
 * Created on 15. Dezember 2014, 15:36
 */

#include "PlayerMovement.h"
#include <math.h>

PlayerMovement::PlayerMovement() :
    _move(0.0), _strafe(0.0), _yaw(0.0), _pitch(0.0),
    _moveVelocity(10.0, 10.0), _strafeVelocity(10.0, 7.0)
{
}

void PlayerMovement::update(Uint32 delta)
{
    _moveVelocity.update(delta);
    _strafeVelocity.update(delta);

    _move = _moveVelocity.velocity() * (GLdouble)delta / 1000.0;
    _strafe = _strafeVelocity.velocity() * (GLdouble)delta / 1000.0;
}

void PlayerMovement::movement(GLdouble& move, GLdouble& strafe)
{
    move = _move;
    strafe = _strafe;
}

void PlayerMovement::rotation(GLdouble& yaw, GLdouble& pitch, GLdouble& roll)
{
    yaw = _yaw;
    pitch = _pitch;
    roll = _strafeVelocity.velocity() / 40.0 * 45.0;
    _yaw = 0.0;
}

void PlayerMovement::speed(GLdouble& speed)
{
    speed = _moveVelocity.velocity() / 15.0
          + _strafeVelocity.velocity() / 14.0;
}

void PlayerMovement::jump()
{
}

void PlayerMovement::stepForward()
{
    _moveVelocity.delta(1.0);
}

void PlayerMovement::stepBackward()
{
    _moveVelocity.delta(-1.0);
}

void PlayerMovement::stepLeft()
{
    _strafeVelocity.delta(-1.0);
}

void PlayerMovement::stepRight()
{
    _strafeVelocity.delta(1.0);
}

void PlayerMovement::rotate(GLdouble yaw, GLdouble pitch)
{
    _yaw = yaw;
    _pitch += pitch;

    if (_pitch < -80.0)
        _pitch = -80.0;
    if (_pitch > 80.0)
        _pitch = 80.0;
}
