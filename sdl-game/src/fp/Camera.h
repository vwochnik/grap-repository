#ifndef CAMERA_H
#define CAMERA_H

#include <OpenGL/gl.h>

class Camera
{
public:
    Camera();
        
    void position(GLdouble, GLdouble, GLdouble);
    void rotation(GLdouble, GLdouble);
    void zoom(GLdouble);
    void applyTransformation();

private:
    GLdouble _x, _y, _z, _yaw, _pitch, _zoom;
};

#endif /* CAMERA_H */
