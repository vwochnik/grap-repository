#include <stdio.h>
#include <math.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include "Camera.h"

Camera::Camera() :
    _x(0.0), _y(0.0), _z(0.0), _yaw(0.0), _pitch(0.0), _zoom(0.0)
{
}

void Camera::position(GLdouble x, GLdouble y, GLdouble z)
{
    _x = x;
    _y = y;
    _z = z;
}

void Camera::rotation(GLdouble yaw, GLdouble pitch)
{
    _yaw = yaw;
    _pitch = pitch;
}

void Camera::zoom(GLdouble zoom)
{
    _zoom = zoom;
}

void Camera::applyTransformation()
{
    GLdouble x, y, z, lx, ly, lz;

    lx = cos(_yaw/180.0*M_PI) * cos(_pitch/180.0*M_PI);
    ly = sin(_pitch/180.0*M_PI);
    lz = sin(_yaw/180.0*M_PI) * cos(_pitch/180.0*M_PI);
    
    x = _x + _zoom*lx;
    y = _y + _zoom*ly;
    z = _z + _zoom*lz;

    gluLookAt(x, y, z, x+lx, y+ly, z+lz, 0.0, 1.0, 0.0);
}
