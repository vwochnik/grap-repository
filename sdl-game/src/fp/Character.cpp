/* 
 * File:   Character.cpp
 * Author: Vincent
 * 
 * Created on 19. Dezember 2014, 16:04
 */

#include "Character.h"
#include "../math/VectorTransformation.h"
#include <OpenGL/gl.h>

Character::Character(GLdouble shoulderHeight, GLdouble neckLength) :
    _shoulderHeight(shoulderHeight), _neckLength(neckLength)
{
}

void Character::update(GLdouble *position, GLdouble *up, GLdouble *front, GLdouble pitch, GLdouble roll)
{
    GLdouble tmp[3];

    VectorTransformation::copy(_position, position);
    VectorTransformation::copy(_up, up);
    VectorTransformation::copy(_front, front);
    _pitch = pitch;
    _roll = roll;

    VectorTransformation::crossProduct(_cross, _front, _up);

    VectorTransformation::rotateAroundAxis(tmp, _up, _cross, pitch);
    VectorTransformation::rotateAroundAxis(_eyeUp, tmp, _front, roll);
    VectorTransformation::rotateAroundAxis(_eyeDirection, _front, _cross, pitch);

    _eyePosition[0] = _position[0] + _up[0]*_shoulderHeight + _eyeUp[0]*_neckLength;
    _eyePosition[1] = _position[1] + _up[1]*_shoulderHeight + _eyeUp[1]*_neckLength;
    _eyePosition[2] = _position[2] + _up[2]*_shoulderHeight + _eyeUp[2]*_neckLength;
}

void Character::eye(GLdouble *position, GLdouble *direction, GLdouble *up)
{
    VectorTransformation::copy(position, _eyePosition);
    VectorTransformation::copy(direction, _eyeDirection);
    VectorTransformation::copy(up, _eyeUp);
}

void Character::display()
{
    GLdouble cross[3];
    
    VectorTransformation::copy(cross, _cross);

    glLineWidth(20.0);
    glColor3d(0.33, 1.0, 0.33);
    glBegin(GL_LINES);
    glVertex3dv(_position);
    glVertex3d(_position[0]+_up[0]*_shoulderHeight, _position[1]+_up[1]*_shoulderHeight, _position[2]+_up[2]*_shoulderHeight);
    glVertex3dv(_position);
    glVertex3d(_position[0]+_front[0]*3.0, _position[1]+_front[1]*3.0, _position[2]+_front[2]*3.0);
    glVertex3dv(_position);
    glVertex3d(_position[0]+cross[0]*3.0, _position[1]+cross[1]*3.0, _position[2]+cross[2]*3.0);
    glEnd();

    glColor3d(0.33, 0.33, 1.0);
    glBegin(GL_LINES);
    glVertex3d(_position[0]+_up[0]*_shoulderHeight, _position[1]+_up[1]*_shoulderHeight, _position[2]+_up[2]*_shoulderHeight);
    glVertex3dv(_eyePosition);
    glVertex3dv(_eyePosition);
    glVertex3d(_eyePosition[0]+_eyeDirection[0]*3.0, _eyePosition[1]+_eyeDirection[1]*3.0, _eyePosition[2]+_eyeDirection[2]*3.0);
    glVertex3dv(_eyePosition);
    glVertex3d(_eyePosition[0]+_eyeUp[0]*3.0, _eyePosition[1]+_eyeUp[1]*3.0, _eyePosition[2]+_eyeUp[2]*3.0);
    glEnd();
}
