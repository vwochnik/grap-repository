/* 
 * File:   Background.cpp
 * Author: Vincent
 * 
 * Created on 22. Dezember 2014, 10:41
 */

#include "Background.h"
#include <OpenGL/gl.h>

Background::Background() :
    Sphere(250.0, 360, true),
    _texture("background", true)
{
}

void Background::display()
{
    glDisable(GL_LIGHTING);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, _texture.id());
    Sphere::display();
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
    glEnable(GL_LIGHTING);
}
