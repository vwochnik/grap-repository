/* 
 * File:   PlayerPosition.h
 * Author: Vincent
 *
 * Created on 15. Dezember 2014, 15:36
 */

#ifndef PLAYERMOVEMENT_H
#define	PLAYERMOVEMENT_H

#include <SDL/SDL_stdinc.h>
#include <OpenGL/gltypes.h>
#include "../math/Velocity.h"

class PlayerMovement
{
public:
    PlayerMovement();
    
    void update(Uint32);
    
    void movement(GLdouble&, GLdouble&);
    void rotation(GLdouble&, GLdouble&, GLdouble&);
    void speed(GLdouble&);

    void jump();
    void stepForward();
    void stepBackward();
    void stepLeft();
    void stepRight();
    void rotate(GLdouble, GLdouble);

private:
    GLdouble _move, _strafe, _yaw, _pitch;
    Velocity _moveVelocity, _strafeVelocity;
};

#endif	/* PLAYERMOVEMENT_H */

