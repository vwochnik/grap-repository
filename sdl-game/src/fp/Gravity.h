/* 
 * File:   Gravity.h
 * Author: Vincent
 *
 * Created on 19. Dezember 2014, 23:31
 */

#ifndef GRAVITY_H
#define	GRAVITY_H

#include <OpenGL/gltypes.h>
#include "Terrain.h"

class Gravity
{
public:
    Gravity(Terrain&);
    
    void gravity(GLdouble*, GLdouble&, GLdouble*);
    void display();

private:
    Terrain& _terrain;
};

#endif	/* GRAVITY_H */

