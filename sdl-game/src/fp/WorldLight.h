/* 
 * File:   WorldLight.h
 * Author: Vincent
 *
 * Created on 18. Dezember 2014, 15:29
 */

#ifndef WORLDLIGHT_H
#define	WORLDLIGHT_H

#include <SDL/SDL_stdinc.h>
#include <OpenGL/gltypes.h>

class WorldLight {
public:
    WorldLight(GLdouble, GLdouble, GLsizei);
    
    void update(Uint32);
    void display();

private:
    GLdouble _distance, _angle, _rotation, _rotation2;
    Uint32 _interval, _passed;
};

#endif	/* WORLDLIGHT_H */

