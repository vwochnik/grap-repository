/* 
 * File:   FirstPersonPerspective.cpp
 * Author: Vincent
 * 
 * Created on 15. Dezember 2014, 16:00
 */

#include "FirstPersonPerspective.h"
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>

FirstPersonPerspective::FirstPersonPerspective(GameContext& context, World& world) :
    _context(context),
    _world(world)
{
}

void FirstPersonPerspective::update(Uint32 delta)
{
}

void FirstPersonPerspective::display()
{
    GLdouble position[3], direction[3], up[3];
    GLdouble ratio;
    
    _world.player().eye(position, direction, up);
    ratio = (GLdouble)_context.width() / (GLdouble)_context.height();

    glViewport(0, 0, (GLsizei)_context.width(), (GLsizei)_context.height()); 
    _world.display(position, direction, up, ratio);
}
