/* 
 * File:   Sphere.h
 * Author: Vincent
 *
 * Created on 19. Dezember 2014, 17:36
 */

#ifndef SPHERE_H
#define	SPHERE_H

#include "../obj/AbstractSphere.h"
#include <SDL/SDL_stdinc.h>
#include <OpenGL/gltypes.h>

class Sphere : public AbstractSphere
{
public:
    Sphere(GLdouble, GLsizei, bool=false);
    
protected:
    void vector(GLdouble*, GLdouble, GLdouble);
    void normal(GLdouble*, GLdouble, GLdouble);
    void texture(GLdouble*, GLdouble, GLdouble);
    
private:
    GLdouble _radius;
};

#endif	/* SPHERE_H */

