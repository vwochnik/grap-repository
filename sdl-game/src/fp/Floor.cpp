/* 
 * File:   Floor.cpp
 * Author: Vincent
 * 
 * Created on 18. Dezember 2014, 15:22
 */

#include "Floor.h"
#include <OpenGL/gl.h>
#include <math.h>

Floor::Floor(GLdouble radius, GLsizei slices, GLsizei stacks) :
    _radius(radius), _slices(slices), _stacks(stacks)
{
}

void Floor::update(Uint32 delta)
{
}

void Floor::display()
{
    GLdouble r, a, dx, dz; GLsizei i, s;

    r = _radius / (GLdouble)_stacks;

    glColor3d(1.0, 1.0, 1.0);

    glBegin(GL_TRIANGLE_FAN);
    glNormal3d(0.0, 1.0, 0.0);
    glVertex3d(0.0, 0.0, 0.0);
    for (i = 0; i <= _slices; i++) {
        dx = sin((GLdouble)i/(GLdouble)_slices*2.0*M_PI);
        dz = cos((GLdouble)i/(GLdouble)_slices*2.0*M_PI);
        glNormal3d(0.0, 1.0, 0.0);
        glVertex3d(r*dx, 0.0, r*dz);
    }
    glEnd();
    
    for (s = 1; s < _stacks; s++) {
        glBegin(GL_TRIANGLE_STRIP);
        for (i = 0; i <= (_slices*2)+1; i++) {
            a = (GLdouble)(i+s-1)/(GLdouble)_slices/2.0;
            dx = sin(a*2.0*M_PI);
            dz = cos(a*2.0*M_PI);

            glNormal3d(0.0, 1.0, 0.0);
            
            if (i % 2 == 0)
                glVertex3d(r*(GLdouble)s*dx, 0.0, r*(GLdouble)s*dz);
            else
                glVertex3d(r*((GLdouble)s+1.0)*dx, 0.0, r*((GLdouble)s+1.0)*dz);
        }
        glEnd();
    }
}
