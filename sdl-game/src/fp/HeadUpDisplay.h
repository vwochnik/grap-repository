/* 
 * File:   HeadUpDisplay.h
 * Author: Vincent
 *
 * Created on 11. Dezember 2014, 09:43
 */

#ifndef HEADUPDISPLAY_H
#define	HEADUPDISPLAY_H

#include "../game/GameContext.h"
#include "Player.h"
#include "GameTimer.h"
#include <SDL/SDL_stdinc.h>

class HeadUpDisplay
{
public:
    HeadUpDisplay(GameContext&, GameTimer&, Player&, GLuint&);
    
    void update(Uint32);
    void display();

protected:
    void displayCrosshair();
    void displayTimer();
    void displayMap();

private:
    GameContext& _context;
    GameTimer& _timer;
    Player& _player;
    GLuint& _mapTexId;
    GLdouble _crosshairFocus;
};

#endif	/* HEADUPDISPLAY_H */

