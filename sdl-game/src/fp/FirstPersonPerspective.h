/* 
 * File:   FirstPersonPerspective.h
 * Author: Vincent
 *
 * Created on 15. Dezember 2014, 16:00
 */

#ifndef FIRSTPERSONPERSPECTIVE_H
#define	FIRSTPERSONPERSPECTIVE_H

#include <SDL/SDL_stdinc.h>
#include "../game/GameContext.h"
#include "Player.h"
#include "World.h"

class FirstPersonPerspective
{
public:
    FirstPersonPerspective(GameContext&, World&);
    
    void update(Uint32);
    void display();

private:
    GameContext& _context;
    World& _world;
};

#endif	/* FIRSTPERSONPERSPECTIVE_H */

