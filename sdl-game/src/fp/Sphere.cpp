/* 
 * File:   Sphere.cpp
 * Author: Vincent
 * 
 * Created on 19. Dezember 2014, 17:36
 */

#include "Sphere.h"
#include <math.h>

Sphere::Sphere(GLdouble radius, GLsizei slices, bool inside) :
    AbstractSphere(slices, inside),
    _radius(radius)
{
}

void Sphere::vector(GLdouble* dst, GLdouble longitude, GLdouble latitude)
{
    dst[0] = _radius * cos(longitude*M_PI/180.0) * fabs(cos(latitude*M_PI/180.0));
    dst[1] = _radius * sin(longitude*M_PI/180.0) * fabs(cos(latitude*M_PI/180.0));
    dst[2] = _radius * sin(latitude*M_PI/180.0);
}

void Sphere::normal(GLdouble* dst, GLdouble longitude, GLdouble latitude)
{
    dst[0] = cos(longitude*M_PI/180.0) * fabs(cos(latitude*M_PI/180.0));
    dst[1] = sin(longitude*M_PI/180.0) * fabs(cos(latitude*M_PI/180.0));
    dst[2] = sin(latitude*M_PI/180.0);
}

void Sphere::texture(GLdouble* dst, GLdouble longitude, GLdouble latitude)
{
    dst[0] = longitude / 360.0;
    dst[1] = (90.0 + latitude) / 180.0;
}
