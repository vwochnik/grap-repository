/* 
 * File:   FirstPersonGameState.cpp
 * Author: Vincent
 * 
 * Created on 9. Dezember 2014, 22:00
 */

#include "FirstPersonGameState.h"
#include <OpenGL/gl.h>

FirstPersonGameState::FirstPersonGameState(GameContext& context) :
    GameState(context),
    _timer(60000),
    _playerControl(_world.player()),
    _firstPersonPerspective(_context, _world),
    _helicopterPerspective(_context, _world, 256),
    _headUpDisplay(_context, _timer, _world.player(), _helicopterPerspective.texId())
{
}

void FirstPersonGameState::enter()
{
    glClearColor(0, 0, 0, 0);
    glClearDepth(1.0f);
    
    _helicopterPerspective.init();
}

void FirstPersonGameState::leave()
{
    _helicopterPerspective.destroy();
}

void FirstPersonGameState::update(Uint32 delta)
{
    _timer.update(delta);
    
    if (_timer.expired() == 1.0)
        return;
    
    _playerControl.update(delta);
    _world.update(delta);
    _firstPersonPerspective.update(delta);
    _helicopterPerspective.update(delta);
    _headUpDisplay.update(delta);
}

void FirstPersonGameState::display()
{
    _firstPersonPerspective.display();
    _helicopterPerspective.display();
    _headUpDisplay.display();
}

void FirstPersonGameState::reshape()
{
}

void FirstPersonGameState::mouseMove(Sint32 xdelta, Sint32 ydelta)
{
    _playerControl.mouseMove(xdelta, ydelta);
}

void FirstPersonGameState::mouseDown(bool alternative)
{
    _playerControl.mouseDown(alternative);
}

void FirstPersonGameState::mouseUp(bool alternative)
{
    _playerControl.mouseUp(alternative);
}

void FirstPersonGameState::keyDown(SDLKey key)
{
    _playerControl.keyDown(key);
}

void FirstPersonGameState::keyUp(SDLKey key)
{
    _playerControl.keyUp(key);
}
