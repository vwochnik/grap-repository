/* 
 * File:   WorldLight.cpp
 * Author: Vincent
 * 
 * Created on 18. Dezember 2014, 15:29
 */

#include "WorldLight.h"
#include <OpenGL/gl.h>
#include <math.h>

#include <stdio.h>

WorldLight::WorldLight(GLdouble distance, GLdouble angle, GLsizei interval) :
    _distance(distance), _angle(angle), _interval(interval), _rotation(0.0), _rotation2(0.0), _passed(0)
{
}

void WorldLight::update(Uint32 delta)
{
    _passed = (_passed + delta) % _interval;

    _rotation = (GLdouble)_passed / (GLdouble)_interval * 360.0;
    _rotation2 = _angle * sin((GLdouble)_passed / (GLdouble)_interval * 2.0*M_PI);
}

void WorldLight::display()
{
    GLdouble x, y, z, dx, dy, dz;

    dx = cos(_rotation/180.0*M_PI) * cos(_rotation2/180.0*M_PI);
    dy = -sin(_rotation2/180.0*M_PI);
    dz = sin(_rotation/180.0*M_PI) * cos(_rotation2/180.0*M_PI);
    
    x = -dx * _distance;
    y = -dy * _distance;
    z = -dz * _distance;
    
    GLfloat light0pos[] = {x, y, z, 1.0};
    GLfloat light0ambient[] = {0.8, 0.8, 1.0, 1.0};

    glEnable(GL_LIGHT0);
    glLightfv(GL_LIGHT0, GL_POSITION, light0pos);
    glLightfv(GL_LIGHT0, GL_AMBIENT, light0ambient);
}
