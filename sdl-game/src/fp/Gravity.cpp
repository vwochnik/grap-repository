/* 
 * File:   Gravity.cpp
 * Author: Vincent
 * 
 * Created on 19. Dezember 2014, 23:31
 */

#include "Gravity.h"
#include <OpenGL/gl.h>
#include <math.h>
#include "../math/VectorTransformation.h"

Gravity::Gravity(Terrain& terrain) :
    _terrain(terrain)
{
}

void Gravity::gravity(GLdouble *up, GLdouble& distance, GLdouble *position)
{
    GLdouble ground[3];
    
    _terrain.ground(ground, up, position);
    
    // the distance from the ground
    distance = VectorTransformation::distance(_terrain.position(), position)
             - VectorTransformation::distance(_terrain.position(), ground);
}

void Gravity::display()
{
}