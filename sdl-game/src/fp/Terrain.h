/* 
 * File:   Terrain.h
 * Author: Vincent
 *
 * Created on 19. Dezember 2014, 21:10
 */

#ifndef TERRAIN_H
#define	TERRAIN_H

#include <SDL/SDL_stdinc.h>
#include <OpenGL/gltypes.h>
#include "../obj/AbstractSphere.h"
#include "../res/ImageTexture.h"

class Terrain : public AbstractSphere
{
public:
    Terrain(GLdouble, GLsizei);
    
    void display();

    void ground(GLdouble*, GLdouble*, GLdouble, GLdouble);
    void ground(GLdouble*, GLdouble*, GLdouble*);

protected:
    void height(GLdouble*, GLdouble, GLdouble);
    void vector(GLdouble*, GLdouble, GLdouble);
    void normal(GLdouble*, GLdouble, GLdouble);
    void texture(GLdouble*, GLdouble, GLdouble);
    void degrees(GLdouble&, GLdouble&, GLdouble*);
    
private:
    GLdouble _radius;
    ImageTexture _texture;
};


#endif	/* TERRAIN_H */

