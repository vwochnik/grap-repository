/* 
 * File:   HeadUpDisplay.cpp
 * Author: Vincent
 * 
 * Created on 11. Dezember 2014, 09:43
 */

#include "HeadUpDisplay.h"
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <math.h>

HeadUpDisplay::HeadUpDisplay(GameContext& context, GameTimer& timer, Player& player, GLuint& mapTexId) :
    _context(context),
    _timer(timer),
    _player(player),
    _mapTexId(mapTexId),
    _crosshairFocus(0.0)
{
}

void HeadUpDisplay::update(Uint32 delta)
{
}

void HeadUpDisplay::display()
{
    glViewport(0, 0, (GLsizei)_context.width(), (GLsizei)_context.height()); 
    glClear(GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0, _context.width(), _context.height(), 0.0, -1.0, 10.0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);
    glDisable(GL_LIGHTING);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

    displayCrosshair();
    displayTimer();
    displayMap();
}

void HeadUpDisplay::displayCrosshair()
{
    GLdouble cx, cy, d;

    cx = _context.width() / 2.0;
    cy = _context.height() / 2.0;
    
    glColor4d(0.8, 0.8, 0.8, 0.1);
    glLineWidth(1.0);
    
    // draw hairs
    glBegin(GL_LINES);
    for (d = 0.0; d < 2* M_PI; d += M_PI_2) {
        glVertex3f(cx + 6.0 * cos(d), cy + 6.0 * sin(d), 0.0);
        glVertex3f(cx + 20.0 * cos(d), cy + 20.0 * sin(d), 0.0);
    }
    glEnd();

    // draw inner dot
    glBegin(GL_POLYGON);
    for (d = 0.0; d < 2* M_PI; d += M_PI_4)
        glVertex3f(cx + 2.0 * cos(d), cy + 2.0 * sin(d), 0.0);
    glEnd();

    // draw outer circle
    glBegin(GL_LINE_LOOP);
    for (d = 0.0; d < 2 * M_PI; d += M_PI_4 / 4.0)
        glVertex3f(cx + (12.0 + 4.0*_crosshairFocus) * cos(d), cy + (12.0 + 4.0*_crosshairFocus) * sin(d), 0.0);
    glEnd();
}

void HeadUpDisplay::displayTimer()
{
    GLdouble cx, cy, r, a, sinA, cosA;
    GLdouble timer, percentage;

    cx = (GLdouble)_context.width() * 0.75;
    cy = (GLdouble)_context.height() * 0.75;
    r = (GLdouble)_context.height() / 6.0 * 1.1;
    
    timer = _timer.expired();
    percentage = (timer == 1.0) ? 1.0 : (1.0 - timer);

    glColor4d(timer, 1.0-timer, 0.0, 0.8);
    glBegin(GL_TRIANGLE_FAN);
    glVertex3d(cx, cy, 0.0);
    for (a = 0.0; a < 360.0*percentage; a += 12.5) {
        sinA = sin(a * M_PI/180.0);
        cosA = cos(a * M_PI/180.0);
        glVertex3d(cx-r*sinA, cy-r*cosA, 0.0);
    }
    sinA = sin(360.0*percentage * M_PI/180.0);
    cosA = cos(360.0*percentage * M_PI/180.0);
    glVertex3d(cx-r*sinA, cy-r*cosA, 0.0);
    glEnd();
}

void HeadUpDisplay::displayMap()
{
    GLdouble cx, cy, r, a, sinA, cosA;

    cx = (GLdouble)_context.width() * 0.75;
    cy = (GLdouble)_context.height() * 0.75;
    r = (GLdouble)_context.height() / 6.0;

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, _mapTexId);
    glBegin(GL_POLYGON);
    for (a = 0.0; a < 360.0; a += 12.5) {
        sinA = sin(a * M_PI/180.0);
        cosA = cos(a * M_PI/180.0);
        glTexCoord2d(0.5+0.5*sinA, 0.5-0.5*cosA);
        glVertex3d(cx+r*sinA, cy+r*cosA, 0.0);
    }
    glEnd();
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
}
