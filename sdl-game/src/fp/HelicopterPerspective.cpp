/* 
 * File:   FirstPersonPerspective.cpp
 * Author: Vincent
 * 
 * Created on 15. Dezember 2014, 16:00
 */

#include "HelicopterPerspective.h"
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <math.h>

HelicopterPerspective::HelicopterPerspective(GameContext& context, World& world, GLsizei size) :
    _context(context),
    _world(world),
    _size(size),
    _glTexture(0), _glFramebuffer(0), _glRenderbuffer(0)
{
}

GLuint& HelicopterPerspective::texId()
{
    return _glTexture;
}

void HelicopterPerspective::init()
{
    glGenFramebuffers(1, &_glFramebuffer);
    glGenTextures(1, &_glTexture);
    
    glBindTexture(GL_TEXTURE_2D, _glTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, (GLsizei)_size, (GLsizei)_size, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);

    glGenRenderbuffers(1, &_glRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _glRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, _size, _size);
}

void HelicopterPerspective::destroy()
{
    glDeleteFramebuffers(1, &_glFramebuffer);
    glDeleteRenderbuffers(1, &_glRenderbuffer);
    glDeleteTextures(1, &_glTexture);
}

void HelicopterPerspective::update(Uint32 delta)
{
}

void HelicopterPerspective::display()
{
    GLdouble position[3], direction[3], up[3];
    GLenum bufs[1];

    glBindFramebuffer(GL_FRAMEBUFFER, _glFramebuffer);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _glTexture, 0);	
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, _glRenderbuffer);
 
    bufs[0] = GL_COLOR_ATTACHMENT0;
    glDrawBuffers(1, bufs);

    _world.player().observer(position, direction, up);

    glViewport(0, 0, _size, _size); 
    _world.display(position, direction, up, 1.0);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    glBindTexture(GL_TEXTURE_2D, _glTexture);
    glGenerateMipmap(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, 0);
}
