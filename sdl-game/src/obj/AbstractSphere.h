/* 
 * File:   AbstractSphere.h
 * Author: Vincent
 *
 * Created on 19. Dezember 2014, 20:15
 */

#ifndef ABSTRACTSPHERE_H
#define	ABSTRACTSPHERE_H

#include <SDL/SDL_stdinc.h>
#include <OpenGL/gltypes.h>

class AbstractSphere
{
public:
    AbstractSphere(GLsizei, bool=false);
    ~AbstractSphere();

    GLdouble * position();
    void position(GLdouble*);
    
    void display();
    
protected:
    virtual void vector(GLdouble*, GLdouble, GLdouble) = 0;
    virtual void normal(GLdouble*, GLdouble, GLdouble) = 0;
    virtual void texture(GLdouble*, GLdouble, GLdouble) = 0;

    void render();
    void renderPoint(GLdouble, GLdouble);

protected:
    GLdouble _position[3];
    GLsizei _slices;
    bool _inside;

private:
    GLuint _list;
    bool _compiled;
};

#endif	/* ABSTRACTSPHERE_H */

