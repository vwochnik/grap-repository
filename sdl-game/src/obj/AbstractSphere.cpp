/* 
 * File:   AbstractSphere.cpp
 * Author: Vincent
 * 
 * Created on 19. Dezember 2014, 20:15
 */

#include "AbstractSphere.h"
#include <OpenGL/gl.h>
#include <math.h>
#include "../math/VectorTransformation.h"

AbstractSphere::AbstractSphere(GLsizei slices, bool inside) :
    _slices(slices), _list(0), _inside(inside), _compiled(false)
{
    _position[0] = _position[1] = _position[2] = 0.0;
}

AbstractSphere::~AbstractSphere()
{
    if (_list != 0)
        glDeleteLists(_list, 1);
}

GLdouble * AbstractSphere::position()
{
    return _position;
}

void AbstractSphere::position(GLdouble *position)
{
    VectorTransformation::copy(_position, position);
}

void AbstractSphere::display()
{
    glPushMatrix();
    glTranslated(_position[0], _position[1], _position[2]);

    if ((_compiled) && (_list != 0)) {
        glCallList(_list);
    } else {
        if (!_compiled)
            _list = glGenLists(1);
        if ((!_compiled) && (_list != 0))
            glNewList(_list, GL_COMPILE_AND_EXECUTE);
        render();
        if ((!_compiled) && (_list != 0))
            glEndList();
        _compiled = true;
    }

    glPopMatrix();
}

void AbstractSphere::render()
{
    GLdouble longitude, latitude, lnginc, latinc;
    GLsizei p;
    
    // twist triangles depending on whether inside or outside sphere
    lnginc = ((_inside) ? 1.0 : -1.0) * 360.0 / (GLdouble)_slices;

    latinc = 360.0 / (GLdouble)_slices;

    glBegin(GL_TRIANGLE_STRIP);
    renderPoint(0, -90.0);
    for(p = 0, longitude = 360.0; p < _slices; p++, longitude += 2.0*lnginc) {
        for(latitude = -90.0+latinc; latitude < 90; latitude += latinc) {
            renderPoint(fmod(longitude, 360.0), latitude);
            renderPoint(fmod(longitude+lnginc, 360.0), latitude);
        }
        renderPoint(0, 90.0);
        for(latitude = 90.0-latinc; latitude > -90; latitude -= latinc) {
            renderPoint(fmod(longitude+lnginc, 360.0), latitude);
            renderPoint(fmod(longitude+2.0*lnginc, 360.0), latitude);
        }
        renderPoint(0, -90.0);
    }
    glEnd();
}

void AbstractSphere::renderPoint(GLdouble longitude, GLdouble latitude)
{
    GLdouble n[3], v[3]; GLdouble t[2];
    
    normal(n, longitude, latitude);
    texture(t, longitude, latitude);
    vector(v, longitude, latitude);
    
    glNormal3dv(n);
    glTexCoord2dv(t);
    glVertex3dv(v);
}
