/* 
 * File:   Application.h
 * Author: Vincent
 *
 * Created on 9. Dezember 2014, 14:55
 */

#ifndef APPLICATION_H
#define	APPLICATION_H

#include "../game/GameState.h"
#include "../game/GameContext.h"
#include "../fp/FirstPersonGameState.h"
#include <stdlib.h>
#include <SDL/SDL.h>

class Application
{
public:
    Application(int width, int height);
    ~Application();
    
    bool init();
    void destroy();
    void run();
    
protected:
    void reshape(int width, int height);

private:
    GameContext _context;
    FirstPersonGameState _state;
    
    SDL_Surface *_surface;
};

#endif	/* APPLICATION_H */

