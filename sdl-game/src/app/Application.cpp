/* 
 * File:   Application.cpp
 * Author: Vincent
 * 
 * Created on 9. Dezember 2014, 14:55
 */

#include "Application.h"
#include "../game/GameContext.h"
#include <OpenGL/gl.h>

// static map of initialization parameters for SDL<->opengl
const struct { SDL_GLattr attr; int value; } __cGlattrs[] = {
    {SDL_GL_RED_SIZE, 8}, {SDL_GL_GREEN_SIZE, 8}, {SDL_GL_BLUE_SIZE, 8},
    {SDL_GL_ALPHA_SIZE, 8}, {SDL_GL_DEPTH_SIZE, 24}, {SDL_GL_BUFFER_SIZE, 32},
    {SDL_GL_ACCUM_RED_SIZE, 8}, {SDL_GL_ACCUM_GREEN_SIZE, 8}, {SDL_GL_ACCUM_BLUE_SIZE, 8},
    {SDL_GL_ACCUM_ALPHA_SIZE, 8}, {SDL_GL_MULTISAMPLEBUFFERS, 1},
    {SDL_GL_MULTISAMPLESAMPLES, 4}, {SDL_GL_DOUBLEBUFFER, 1}, {(SDL_GLattr)-1, 0}
};

Application::Application(int width, int height) :
    _surface(NULL),
    _context(width, height, 40),
    _state(_context)
{
}

Application::~Application()
{
}

bool Application::init()
{
    size_t i;

    if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
        goto error;

    // initialize OpenGL attributes
    for (i = 0; (int)__cGlattrs[i].attr != -1; i++)
        SDL_GL_SetAttribute(__cGlattrs[i].attr, __cGlattrs[i].value);

    // initialize surface first time
    if ((_surface = SDL_SetVideoMode(_context._width, _context._height, 32, SDL_HWSURFACE | SDL_GL_DOUBLEBUFFER | SDL_OPENGL | SDL_RESIZABLE)) == NULL)
        goto error;

    SDL_WM_GrabInput(SDL_GRAB_ON);
    SDL_ShowCursor(0);
    
    glEnable(GL_MULTISAMPLE);
    _state.enter();
    reshape(_context._width, _context._height);
    return true;

error:
    SDL_Quit();
    return false;
}

void Application::destroy()
{
    _state.leave();
    SDL_ShowCursor(1);
    SDL_WM_GrabInput(SDL_GRAB_OFF);
    SDL_Quit();
}

void Application::run()
{
    bool done = false;
    SDL_Event event;
    Uint32 currentTicks, lastTicks = 0, frameTime, delta;
    
    while (!done) {
        currentTicks = SDL_GetTicks();
        if (lastTicks == 0)
            lastTicks = currentTicks;

        while (SDL_PollEvent(&event)) {
            switch (event.type) {
            case SDL_QUIT:
                done = true;
                break;
            case SDL_VIDEORESIZE:
                reshape(event.resize.w, event.resize.h);
                break;
            case SDL_MOUSEMOTION:
                _state.mouseMove(event.motion.xrel, event.motion.yrel);
                break;
            case SDL_MOUSEBUTTONDOWN:
                if ((event.button.button == SDL_BUTTON_LEFT) || (event.button.button == SDL_BUTTON_RIGHT))
                    _state.mouseDown(event.button.button == SDL_BUTTON_RIGHT);
                break;
            case SDL_MOUSEBUTTONUP:
                if ((event.button.button == SDL_BUTTON_LEFT) || (event.button.button == SDL_BUTTON_RIGHT))
                    _state.mouseUp(event.button.button == SDL_BUTTON_RIGHT);
                break;
            case SDL_KEYDOWN:
                _state.keyDown(event.key.keysym.sym);
                break;
            case SDL_KEYUP:
                _state.keyUp(event.key.keysym.sym);
                break;
            }
        }

        delta = currentTicks - lastTicks;
        _state.update(delta);
        _state.display();
        SDL_GL_SwapBuffers();
        
        if ((frameTime = SDL_GetTicks() - currentTicks) < 1000 / _context._fps)
            SDL_Delay((1000 / _context._fps) - frameTime);

        lastTicks = currentTicks;
    }
}

void Application::reshape(int width, int height)
{
    SDL_Surface *surface;

    if ((width != _context._width) || (height != _context._height)) {
        if ((surface = SDL_SetVideoMode(width, height, 32, SDL_HWSURFACE | SDL_GL_DOUBLEBUFFER | SDL_OPENGL | SDL_RESIZABLE)) == NULL)
            return;
        
        _context._width = width;
        _context._height = height;
        _surface = surface;
    }
    
    _state.reshape();
}
