/* 
 * File:   ImageTexture.h
 * Author: Vincent
 *
 * Created on 20. Dezember 2014, 15:32
 */

#ifndef IMAGETEXTURE_H
#define	IMAGETEXTURE_H

#include <OpenGL/gltypes.h>

class ImageTexture
{
public:
    ImageTexture(const char*, bool=false);
    ~ImageTexture();
    
    GLuint id();

protected:
    bool load();

private:
    const char *_name;
    GLuint _id;
    bool _loaded, _repeat;
};

#endif	/* IMAGETEXTURE_H */

