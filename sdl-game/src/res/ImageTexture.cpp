/* 
 * File:   ImageTexture.cpp
 * Author: Vincent
 * 
 * Created on 20. Dezember 2014, 15:32
 */

#include "ImageTexture.h"
#include <OpenGL/gl.h>
#include <SOIL/SOIL.h>
#include <stdio.h>

#define PATH_MAX 255

ImageTexture::ImageTexture(const char *name, bool repeat) :
    _name(name), _id(0), _loaded(false), _repeat(false)
{
}

ImageTexture::~ImageTexture()
{
    if (_loaded)
        glDeleteTextures(1, &_id);
}
    
GLuint ImageTexture::id()
{
    if (!_loaded)
        load();

    return _id;
}

bool ImageTexture::load()
{
    char path[PATH_MAX + 1];
    int flags;
    
    flags = SOIL_FLAG_MIPMAPS | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT;
    if (_repeat)
        flags |= SOIL_FLAG_TEXTURE_REPEATS;

    snprintf(path, PATH_MAX, "textures/%s.png", _name);
    _id = SOIL_load_OGL_texture(path, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, flags);
    
    _loaded = true;
}
