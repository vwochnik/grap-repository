/* 
 * File:   FractalTreeGenerator.cpp
 * Author: Vincent
 * 
 * Created on 17. Dezember 2014, 19:54
 */

#include "FractalTreeGenerator.h"
#include "VectorTransformation.h"
#include <math.h>
#include <stdio.h>

FractalTreeGenerator::FractalTreeGenerator(GLsizei depth, GLsizei branches) :
    _depth(depth), _branches(branches), _data(NULL), _length(0), _count(0), _distance(0.0)
{
}

FractalTreeGenerator::~FractalTreeGenerator()
{
    if (_data != NULL)
        free(_data);
}

bool FractalTreeGenerator::generate(GLdouble *position, GLdouble *up, GLdouble *front, GLdouble length, GLdouble angle, GLdouble factor)
{
    GLsizei i;
    
    if (_data == NULL) {
        for (i = 1, _length = 1; i < _depth; i++)
            _length += pow(_branches, i);
        _data = (FractalTreeGenerator::Fractal*)malloc(_length*sizeof(FractalTreeGenerator::Fractal));
        if (_data == NULL)
            return false;
    }
        
    _count = 0;
    for (i = 0, _distance = 0.0; i < _depth; i++)
        _distance += pow(factor, i) * length;
    rfractal(position, up, front, length, angle, 0.0, factor, 0);
    
    return _count == _length;
}

FractalTreeGenerator::Fractal* FractalTreeGenerator::fractals()
{
    return _data;
}

size_t FractalTreeGenerator::count()
{
    return _count;
}

GLdouble FractalTreeGenerator::distance()
{
    return _distance;
}

void FractalTreeGenerator::rfractal(GLdouble *position, GLdouble *up, GLdouble *front, GLdouble length, GLdouble angle, GLdouble distance, GLdouble factor, GLsizei depth)
{
    GLdouble r; GLsizei i;
    GLdouble destination[3], ortho[3], tmp[3];
    
    for (i = 0; i < 3; i++)
        destination[i] = position[i] + length*up[i];
    
    addfrac(position, up, front, length, distance);
    
    if (++depth == _depth)
        return;
    
    // calculate a normalized cross product to get the orthogonal
    VectorTransformation::crossProduct(tmp, up, front);
    VectorTransformation::normalize(ortho, tmp);

    r = 360.0 / (GLdouble)_branches;
    for (i = 0; i < _branches; i++) {
        GLdouble up2[3], front2[3];

        // rotate the up vector
        VectorTransformation::rotateAroundAxis(tmp, up, ortho, angle);
        VectorTransformation::rotateAroundAxis(up2, tmp, up, 0.5*r+(GLdouble)i*r);

        // rotate the front vector
        VectorTransformation::rotateAroundAxis(tmp, front, ortho, angle);
        VectorTransformation::rotateAroundAxis(front2, tmp, up, 0.5*r+(GLdouble)i*r);

        rfractal(destination, up2, front2, length*factor, angle, distance+length, factor, depth);
    }
}

void FractalTreeGenerator::addfrac(GLdouble *position, GLdouble *up, GLdouble *front, GLdouble length, GLdouble distance)
{
    FractalTreeGenerator::Fractal *frac;
    
    frac = &_data[_count++];
    
    VectorTransformation::copy(frac->position, position);
    VectorTransformation::copy(frac->up, up);
    VectorTransformation::copy(frac->front, front);
    frac->length = length;
    frac->distance = distance;
}
