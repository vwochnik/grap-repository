/* 
 * File:   Velocity.h
 * Author: Vincent
 *
 * Created on 15. Dezember 2014, 14:34
 */

#ifndef VELOCITY_H
#define	VELOCITY_H

#include <OpenGL/gltypes.h>
#include <SDL/SDL_stdinc.h>

class Velocity {
public:
    Velocity(GLdouble, GLdouble);
    
    void update(Uint32);

    GLdouble velocity();
    void delta(GLdouble);
    void stop();

protected:
    void zero(GLdouble&);

private:
    GLdouble _friction, _limit, _velocity, _delta;
};

#endif	/* VELOCITY_H */

