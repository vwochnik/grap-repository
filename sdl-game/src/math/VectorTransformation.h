/* 
 * File:   VectorTransformation.h
 * Author: Vincent
 *
 * Created on 16. Dezember 2014, 23:18
 */

#ifndef VECTORTRANSFORMATION_H
#define	VECTORTRANSFORMATION_H

#include <OpenGL/gltypes.h>

class VectorTransformation
{
private:
    VectorTransformation();

public:
    static void copy(GLdouble*, GLdouble*);
    static void orthogonal(GLdouble*, GLdouble*);
    static void crossProduct(GLdouble*, GLdouble*, GLdouble*);
    static void normalize(GLdouble*, GLdouble*);
    static void rotateAroundAxis(GLdouble*, GLdouble*, GLdouble*, GLdouble);
    static GLdouble distance(GLdouble*, GLdouble*);
};

#endif	/* VECTORTRANSFORMATION_H */

