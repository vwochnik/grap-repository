/* 
 * File:   VectorTransformation.cpp
 * Author: Vincent
 * 
 * Created on 16. Dezember 2014, 23:18
 */

#include "VectorTransformation.h"
#include <math.h>

inline void set(GLdouble *v, GLdouble x, GLdouble y, GLdouble z)
{
    v[0] = x;
    v[1] = y;
    v[2] = z;
}

void VectorTransformation::copy(GLdouble *dst, GLdouble *src)
{
    GLsizei i;
    
    for (i = 0; i < 3; i++)
        dst[i] = src[i];
}

void VectorTransformation::orthogonal(GLdouble *dst, GLdouble *src)
{
    GLdouble a, b, c;
    GLdouble tmp[3];

    a = 0.5; b = 0.5;
    if (src[2] != 0.0)
        c = (-src[0] * a - src[1] * b) / src[2];
    else if (src[1] != 0.0)
        c = (-src[0] * a -src[2] * b) / src[1];
    else if (src[0] != 0.0)
        c = (-src[1] * a - src[2] * b) / src[0];

    if (src[2] != 0.0) {
        set(tmp, a, b, c);
    } else if (src[1] != 0.0) {
        set(tmp, a, c, b);
    } else if (src[0] != 0.0) {
        set(tmp, b, c, a);
    } else {
        set(tmp, 0.0, 0.0, 0.0);
    }
    
    normalize(dst, tmp);
}

void VectorTransformation::crossProduct(GLdouble *dst, GLdouble *src1, GLdouble *src2)
{
    dst[0] = src1[1] * src2[2] - src2[1] * src1[2];
    dst[1] = src1[2] * src2[0] - src2[2] * src1[0];
    dst[2] = src1[0] * src2[1] - src2[0] * src1[1];
}

void VectorTransformation::normalize(GLdouble *dst, GLdouble *src)
{
    GLdouble l;
    
    l = sqrt(pow(src[0], 2.0) + pow(src[1], 2.0) + pow(src[2], 2.0));
    
    dst[0] = src[0] / l;
    dst[1] = src[1] / l;
    dst[2] = src[2] / l;
}

void VectorTransformation::rotateAroundAxis(GLdouble *dst, GLdouble *src, GLdouble *axis, GLdouble a)
{
    GLdouble sinA, cosA; GLsizei i;
    
    sinA = sin(a/180.0*M_PI);
    cosA = cos(a/180.0*M_PI);
    
    GLdouble matrix[3][3] = {
        {cosA+pow(axis[0], 2.0)*(1-cosA), axis[0]*axis[1]*(1-cosA)-axis[2]*sinA, axis[0]*axis[2]*(1-cosA)+axis[1]*sinA},
        {axis[0]*axis[1]*(1-cosA)+axis[2]*sinA, cosA+pow(axis[1], 2.0)*(1-cosA), axis[1]*axis[2]*(1-cosA)-axis[0]*sinA},
        {axis[2]*axis[0]*(1-cosA)-axis[1]*sinA, axis[2]*axis[1]*(1-cosA)+axis[0]*sinA, cosA+pow(axis[2], 2.0)*(1-cosA)}};
    
    // apply matrix transformation
    for (i = 0; i < 3; i++)
        dst[i] = src[0]*matrix[i][0] + src[1]*matrix[i][1] + src[2]*matrix[i][2];
}

GLdouble VectorTransformation::distance(GLdouble *a, GLdouble *b)
{
    return sqrt(pow(b[0]-a[0], 2.0) + pow(b[1]-a[1], 2.0) + pow(b[2]-a[2], 2.0));
}
