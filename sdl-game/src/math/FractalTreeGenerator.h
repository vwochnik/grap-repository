/* 
 * File:   FractalTreeGenerator.h
 * Author: Vincent
 *
 * Created on 17. Dezember 2014, 19:54
 */

#ifndef FRACTALTREEGENERATOR_H
#define	FRACTALTREEGENERATOR_H

#include <stdlib.h>
#include <OpenGL/gltypes.h>

class FractalTreeGenerator {
public:
    FractalTreeGenerator(GLsizei, GLsizei);
    ~FractalTreeGenerator();
    
    struct Fractal {
        GLdouble position[3];
        GLdouble up[3];
        GLdouble front[3];
        GLdouble length;
        GLdouble distance;
    };

    bool generate(GLdouble*, GLdouble*, GLdouble*, GLdouble, GLdouble, GLdouble);

    Fractal* fractals();
    size_t count();
    GLdouble distance();

    void rfractal(GLdouble*, GLdouble*, GLdouble*, GLdouble, GLdouble, GLdouble, GLdouble, GLsizei);
    void addfrac(GLdouble*, GLdouble*, GLdouble*, GLdouble, GLdouble);

private:
    GLsizei _depth, _branches;
    
    Fractal *_data;
    size_t _count, _length;
    GLdouble _distance;
};

#endif	/* FRACTALTREEGENERATOR_H */

