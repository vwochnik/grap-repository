/* 
 * File:   Velocity.cpp
 * Author: Vincent
 * 
 * Created on 15. Dezember 2014, 14:34
 */

#include "Velocity.h"
#include <math.h>
#include <float.h>

Velocity::Velocity(GLdouble friction, GLdouble limit) :
    _friction(friction),
    _limit(limit),
    _velocity(0.0),
    _delta(0.0)
{
}

void Velocity::update(Uint32 delta)
{
    GLdouble multiplier;
    
    multiplier = 1.0 - _friction*(GLdouble)delta/1000.0;
    if (multiplier < 0.0)
        multiplier = 0.0;
    
    _velocity = _velocity * multiplier + _delta * _limit * (1.0 - multiplier);
    _delta *= multiplier;
    
    if (_velocity > _limit)
        _velocity = _limit;
    if (_velocity < -_limit)
        _velocity = -_limit;

    zero(_velocity);
    zero(_delta);
}

GLdouble Velocity::velocity()
{
    return _velocity;
}

void Velocity::delta(GLdouble value)
{
    _delta += value;
    
    if (_delta > 1.0)
        _delta = 1.0;
    if (_delta < -1.0)
        _delta = -1.0;
}

void Velocity::stop()
{
    _velocity = 0.0;
    _delta = 0.0;
}

void Velocity::zero(GLdouble& d)
{
    if (((d > 0.0) && (d < DBL_EPSILON)) ||
        ((d < 0.0) && (d > -DBL_EPSILON))) {
        d = 0.0;
    }
}
