/* 
 * File:   main.cpp
 * Author: Vincent Wochnik <v.wochnik@gmail.com>
 *
 * Created on 11. September 2014, 14:04
 */
#include <stdlib.h>
#include <math.h>
#include <GLUT/glut.h>

#define MENU_TOGGLE_OUTLINE 1
#define MENU_TOGGLE_FILL 2
#define MENU_TOGGLE_DIAGONALS 3

bool outline = true, fill = false, diagonals = true;

GLfloat vertices[] = {
    -0.8f, -0.8f, 0.0f,
    0.8f, -0.8f, 0.0f,
    0.8f, 0.8f, 0.0f,
    -0.8f, 0.8f, 0.0f
};

GLfloat vertices2[] = {
    -0.8f, -0.8f, 0.0f,
    0.8f, 0.8f, 0.0f,
    -0.8f, 0.8f, 0.0f,
    0.8f, -0.8f, 0.0f
};

GLfloat colors[] = {
    1.0, 0.0, 0.0,
    0.5, 0.5, 0.0,
    0.0, 0.1, 0.0,
    0.0, 0.5, 0.5
};

void init(void) {
    glClearColor(0.85, 0.85, 0.9, 1.0);
}

void display(void) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);

    glPushMatrix();

    glTranslatef(0,0,-2);
    

    if (fill) {
        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_COLOR_ARRAY);
        glColorPointer(3, GL_FLOAT, 0, colors);
        glVertexPointer(3, GL_FLOAT, 0, vertices);
        glDrawArrays(GL_POLYGON, 0, 4);
        glDisableClientState(GL_VERTEX_ARRAY);
        glDisableClientState(GL_COLOR_ARRAY);
    }
    
    if (diagonals) {
        glEnableClientState(GL_VERTEX_ARRAY);
        glEnable(GL_LINE_STIPPLE);
        glLineWidth(1.0);
        glLineStipple(4, 0xAAAA);
        glColor3f(0.3, 0.3, 0.3);
        glVertexPointer(3, GL_FLOAT, 0, vertices2);
        glDrawArrays(GL_LINES, 0, 4);
        glDisableClientState(GL_VERTEX_ARRAY);
        glDisable(GL_LINE_STIPPLE);
    }
    
    if (outline)
        glLineWidth(2.0);
    else
        glLineWidth(1.0);

    glEnableClientState(GL_VERTEX_ARRAY);
    glColor3f(0.0, 0.0, 0.0);
    glVertexPointer(3, GL_FLOAT, 0, vertices);
    glDrawArrays(GL_LINE_LOOP, 0, 4);
    glDisableClientState(GL_VERTEX_ARRAY);

    glPopMatrix();
    glFlush();
}

void reshape(int w, int h) {
    glViewport(0, 0, (GLsizei) w, (GLsizei) h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, (GLfloat) w / (GLfloat) h, 1.0, 20.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void mouse(int button, int state, int x, int y) {
/*    switch (button) {
        case GLUT_LEFT_BUTTON:
            if (state == GLUT_DOWN)
            break;
        case GLUT_RIGHT_BUTTON:
            if (state == GLUT_DOWN)
            break;
        default:
            break;
    }*/
}

void keyboard(unsigned char key, int x, int y) {
    switch (key) {
        case 27:
            exit(0);
            break;
    }
}

void menu(int value) {
    switch (value) {
        case MENU_TOGGLE_OUTLINE:
            outline = !outline;
            glutPostRedisplay();
            break;
        case MENU_TOGGLE_FILL:
            fill = !fill;
            glutPostRedisplay();
            break;
        case MENU_TOGGLE_DIAGONALS:
            diagonals = !diagonals;
            glutPostRedisplay();
            break;
    }
}

int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(500, 500);
    glutInitWindowPosition(100, 100);
    glutCreateWindow(argv[0]);
    init();
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutMouseFunc(mouse);
    glutKeyboardFunc(keyboard);
    
    glutCreateMenu(menu);
    glutAddMenuEntry("Toggle outline", MENU_TOGGLE_OUTLINE);
    glutAddMenuEntry("Toggle fill", MENU_TOGGLE_FILL);
    glutAddMenuEntry("Toggle diagonals", MENU_TOGGLE_DIAGONALS);
    glutAttachMenu(GLUT_RIGHT_BUTTON);
    
    glutMainLoop();
    return 0;
}
